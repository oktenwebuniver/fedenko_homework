// Треба реалізувати свій розпорядок дня.
// Колбеками, промісами та асинк авейт.

// В дні має бути від 7 до 10 подій. Всі події мають мати описані успішні та не успішні варіанти виконання.
// Має бути так
// 1) прокинувся
// 2) Поснідав
// 3) почистав зуби
// і т.д.

// Якщо щось пішло не так (нема шо їсти), то має бути викинута помилка і решта функцій виконуватись не мають.
// Якщо ж все ок, то ви маєте прожити свій звичайний день.
// Кожна подія має бути з рандомною (не по зростанню) затримкою.

//===========  callback  ==============

// let i = 0;
// function dayFedenko(wakeUp) {
//   console.log('В даний момент я ще сплю...ZzZ');
//   setTimeout(() => {
//     i = Math.random();
//     if (i > 0.05) {
//       wakeUp(null, 'Добреньке утречко! Я прокинувся.');
//       return;
//     }
//     wakeUp('Сорі, ще посплю. До ранку робив таски.', null);
//   }, 1000);
// }

// function breakfest(cb) {
//   console.log('________');
//   console.log('Йду снідати...');
//   setTimeout(() => {
//     i = Math.random();
//     if (i > 0.05) {
//       cb(null, 'ммм.. Дуже смачна перловка!');
//       return;
//     }
//     cb('Не знайшов тарілку..(((', null);
//   }, 1500);
// }

// function brushMyTeeth(cb) {
//   console.log('________');
//   console.log('потрібно почистити зуби ...');
//   setTimeout(() => {
//     i = Math.random();
//     if (i > 0.1) {
//       cb(null, 'Почистив.');
//       return;
//     }
//     cb('Закінчилася паста,', null);
//   }, 1000);
// }

// function closeMyApartament(cb) {
//   console.log('________');
//   console.log('Збираюся на роботу...');
//   setTimeout(() => {
//     i = Math.random();
//     if (i > 0.08) {
//       cb(null, 'Чудовий сьогодні деньок..');
//       return;
//     }
//     cb('Не знайшов ключі.. Доведеться залишитись вдома((', null);
//   }, 1000);
// }

// function ariveToWork(cb) {
//   console.log('________');
//   console.log('Пригнув в тачку, їду на роботу...');
//   setTimeout(() => {
//     i = Math.random();
//     if (i > 0.1) {
//       cb(null, 'Доїхав, кльова в мене тачка!');
//       return;
//     }
//     cb('Зламалась моя малишка, потрібно купувати нове авто((', null);
//   }, 2000);
// }

// function askForRaise(cb) {
//   console.log('________');
//   console.log('Здається бос сьогодні в гарному настрої, попрошу підвищення.');
//   setTimeout(() => {
//     i = Math.random();
//     if (i > 0.1) {
//       cb(null, 'УРА, підвищили!');
//       return;
//     }
//     cb('Мене звільнили(((', null);
//   }, 1000);
// }

// function celebrateWithFriends(cb) {
//   console.log('________');
//   console.log('Запропоную друзям відсвяткувати підвищення в барі...');
//   setTimeout(() => {
//     i = Math.random();
//     if (i > 0.15) {
//       cb(null, 'Згодні, зустрінемося на вихідних.');
//       return;
//     }
//     cb('Які гульки, КАРАНТИН (((', null);
//   }, 1500);
// }

// function endWork(cb) {
//   console.log('________');
//   console.log('Закінчується робочий день, пора збиратись ...');
//   setTimeout(() => {
//     i = Math.random();
//     if (i > 0.3) {
//       cb(null, 'Вийшов з роботи, все гуд!');
//       return;
//     }
//     cb('Бос  Г І В Н Ю К,  повісив ще одну таску (((', null);
//   }, 1000);
// }

// function backToHome(cb) {
//   console.log('________');
//   console.log('Пригнув в тачку, їду в мій дворець ...');
//   setTimeout(() => {
//     i = Math.random();
//     if (i > 0.2) {
//       cb(null, 'Вже вдома.');
//       return;
//     }
//     cb('Зламалась моя малишка, потрібно купувати нове авто((', null);
//   }, 2000);
// }

// function theEnd() {
//   console.log('-----  T H E   E N D  -----');
// }

// dayFedenko((error, success) => {
//   if (error) {
//     console.log(error);
//     theEnd();
//     return;
//   }
//   console.log(success);
//   breakfest((err, succ) => {
//     if (err) {
//       console.log(err);
//       theEnd();
//       return;
//     }
//     console.log(succ);
//     brushMyTeeth((err, succ) => {
//       if (err) {
//         console.log(err);
//         theEnd();
//         return;
//       }
//       console.log(succ);
//       closeMyApartament((err, succ) => {
//         if (err) {
//           console.log(err);
//           theEnd();
//           return;
//         }
//         console.log(succ);
//         ariveToWork((err, succ) => {
//           if (err) {
//             console.log(err);
//             theEnd();
//           }
//           console.log(succ);
//           askForRaise((err, succ) => {
//             if (err) {
//               console.log(err);
//               theEnd();
//               return;
//             }
//             console.log(succ);
//             celebrateWithFriends((err, succ) => {
//               if (err) {
//                 console.log(err);
//                 theEnd();
//                 return;
//               }
//               console.log(succ);
//               endWork((err, succ) => {
//                 if (err) {
//                   console.log(err);
//                   theEnd();
//                   return;
//                 }
//                 console.log(succ);

//                 backToHome((err, succ) => {
//                   if (err) {
//                     console.log(err);
//                     theEnd();
//                     return;
//                   }
//                   console.log(succ);
//                   theEnd();
//                 });
//               });
//             });
//           });
//         });
//       });
//     });
//   });
// });

// ===========  promis  ==============

// function dayFedenko() {
//   return new Promise((resolve, reject) => {
//     console.log('В даний момент я ще сплю...ZzZ');
//     setTimeout(() => {
//       i = Math.random();
//       if (i > 0.05) {
//         resolve('Добреньке утречко! Я прокинувся.');
//       }
//       reject('Сорі, ще посплю. До ранку робив таски.');
//     }, 1000);
//   });
// }

// function breakfest() {
//   return new Promise((resolve, reject) => {
//     console.log('________');
//     console.log('Йду снідати...');
//     setTimeout(() => {
//       i = Math.random();
//       if (i > 0.05) {
//         resolve('ммм.. Дуже смачна перловка!');
//       }
//       reject('Не знайшов тарілку..(((');
//     }, 1000);
//   });
// }

// function brushMyTeeth() {
//   return new Promise((resolve, reject) => {
//     console.log('________');
//     console.log('потрібно почистити зуби ...');
//     setTimeout(() => {
//       i = Math.random();
//       if (i > 0.1) {
//         resolve('Почистив.');
//       }
//       reject('Закінчилася паста, немає бажання продовжувати цей день!');
//     }, 1000);
//   });
// }

// function closeMyApartament() {
//   return new Promise((resolve, reject) => {
//     console.log('________');
//     console.log('Збираюся на роботу...');
//     setTimeout(() => {
//       i = Math.random();
//       if (i > 0.08) {
//         resolve('Чудовий сьогодні деньок..');
//       }
//       reject('Не знайшов ключі.. Доведеться залишитись вдома((');
//     }, 1000);
//   });
// }

// function ariveToWork() {
//   return new Promise((resolve, reject) => {
//     console.log('________');
//     console.log('Пригнув в тачку, їду на роботу...');
//     setTimeout(() => {
//       i = Math.random();
//       if (i > 0.1) {
//         resolve('Доїхав, кльова в мене тачка!');
//         return;
//       }
//       reject('Зламалась моя малишка, потрібно купувати нове авто((');
//     }, 1500);
//   });
// }

// function askForRaise() {
//   return new Promise((resolve, reject) => {
//     console.log('________');
//     console.log('Здається бос сьогодні в гарному настрої, попрошу підвищення.');
//     setTimeout(() => {
//       i = Math.random();
//       if (i > 0.1) {
//         resolve('УРА, підвищили!');
//         return;
//       }
//       reject('Мене звільнили(((');
//     }, 1000);
//   });
// }

// function celebrateWithFriends() {
//   return new Promise((resolve, reject) => {
//     console.log('________');
//     console.log('Запропоную друзям відсвяткувати підвищення в барі...');
//     setTimeout(() => {
//       i = Math.random();
//       if (i > 0.15) {
//         resolve('Згодні, зустрінемося на вихідних.');
//         return;
//       }
//       reject('Які гульки, КАРАНТИН (((');
//     }, 1500);
//   });
// }

// function endWork() {
//   return new Promise((resolve, reject) => {
//     console.log('________');
//     console.log('Закінчується робочий день, пора збиратись ...');
//     setTimeout(() => {
//       i = Math.random();
//       if (i > 0.3) {
//         resolve('Вийшов з роботи, все гуд!');
//       }
//       reject('Бос  Г І В Н Ю К,  повісив ще одну таску (((', null);
//     }, 1000);
//   });
// }

// function backToHome() {
//   return new Promise((resolve, reject) => {
//     console.log('________');
//     console.log('Пригнув в тачку, їду в мій дворець ...');
//     setTimeout(() => {
//       i = Math.random();
//       if (i > 0.2) {
//         resolve('Вже вдома.');
//       }
//       reject('Зламалась моя малишка, потрібно купувати нове авто((');
//     }, 1500);
//   });
// }

// function theEnd() {
//   setTimeout(() => {
//     console.log('-----  T H E   E N D  -----');
//   }, 1000);
// }

// dayFedenko()
//   .then((value) => {
//     console.log(value);
//     return breakfest();
//   })
//   .then((value) => {
//     console.log(value);
//     return brushMyTeeth();
//   })
//   .then((value) => {
//     console.log(value);
//     return closeMyApartament();
//   })
//   .then((value) => {
//     console.log(value);
//     return ariveToWork();
//   })
//   .then((value) => {
//     console.log(value);
//     return askForRaise();
//   })
//   .then((value) => {
//     console.log(value);
//     return celebrateWithFriends();
//   })
//   .then((value) => {
//     console.log(value);
//     return endWork();
//   })
//   .then((value) => {
//     console.log(value);
//     return backToHome();
//   })
//   .then((value) => {
//     console.log(value);
//   })
//   .catch((value) => {
//     console.log(value);
//   })
//   .finally(() => {
//     theEnd();
//   });

//

//

//===== DOM ======

// const btnToParent = document.getElementById('toParent');
// const btnToChildren = document.getElementById('toChildren');
// const conteinerBady = document.getElementById('conteinerBady');
// const allElements = conteinerBady.querySelectorAll('*');

// let i = 0;

// btnToParent.onclick = () => {
//   i++;
//   btnToChildren.classList.remove('none');
//   for (const key in allElements) {
//     const element = allElements[key];
//     element.parentElement.classList.remove('hover');
//     element.classList.remove('child');
//     if (+key === i) {
//       element.parentElement.classList.add('hover');
//       element.classList.add('child');
//       if (+key + 1 >= allElements.length) {
//         element.parentElement.classList.add('hover');
//         btnToParent.classList.add('none');
//       }
//       return;
//     }
//   }
// };

// btnToChildren.onclick = () => {
//   i--;
//   btnToParent.classList.remove('none');
//   for (const key in allElements) {
//     const element = allElements[key];
//     element.parentElement.classList.remove('hover');
//     element.classList.remove('child');
//     if (+key === i) {
//       element.parentElement.classList.add('hover');
//       element.classList.add('child');
//       if (+key === 0) {
//         element.parentElement.classList.add('hover');
//         btnToChildren.classList.add('none');
//       }
//       return;
//     }
//   }
// };

// function user() {
//   let _login = 'feden';
//   let _pass = '12345';

//   let loginUser = (log, pas) => {
//     if (_login === log && _pass === pas) {
//       console.log('welcome');
//     } else {
//       console.log('try again pls');
//     }
//   };

//   return { loginUser };
// }

// let fedenko = user();
// fedenko.loginUser('feden', '12345');

// function cuntor() {
//   let num = 0;
//   function multiplay() {
//     console.log(num);
//     num++;
//   }
//   return { multiplay };
// }

// let newNum = cuntor();
// newNum.multiplay();
// newNum.multiplay();
// newNum.multiplay();
// newNum.multiplay();
// newNum.multiplay();

// function findNb(m) {
//   let n = 1;
//   let k = 0;
//   k = k + Math.pow(n, 3);

//   if (k < m) {
//     n++;
//     findNb(m);
//   } else if (k === m) {
//     console.log(n);
//     return n;
//   } else {
//     return -1;
//   }
// }

// function findNb(m) {
//   for (let n = 0; ; n++) {
//     if (m > 0) {
//       let currCubeVol = Math.pow(n + 1, 3);

//       m = m - currCubeVol;
//     } else if (m === 0) {
//       return n;
//     } else if (m < 0) {
//       return -1;
//     }
//   }
// }

// // findNb(49);
// let i = findNb(4183059834009);
// console.log(i);
// let numbers = [1, 2, 3, 4, 5];
// let newArr = JSON.parse(JSON.stringify(numbers));
// console.log(newArr);
// let littleNum = Math.min(...newArr);

// const n = newArr.findIndex(value => value === littleNum);
// newArr.slice(n, 1);
// console.log(newArr);