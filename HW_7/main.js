// - Создать произвольный елемент с id = text.  Используя JavaScript, сделайте так, чтобы при
//   клике на кнопку исчезал элемент с id="text".

// const div = document.createElement('div');
// const btn = document.createElement('button');
// btn.innerText = 'Remove TexT';
// div.setAttribute('id', 'text');
// div.classList.add('divTask1');
// document.body.appendChild(div);
// document.body.appendChild(btn);

// btn.onclick = () => {
//     div.style.display = 'none';
// };

//

//

// - Создайте кнопку, при клике на которую, она будет скрывать сама себя.

// const btnSomeNone = document.createElement('button');
// btnSomeNone.innerText = 'Скрой меня =)';
// document.body.appendChild(btnSomeNone);
// btnSomeNone.onclick = () => {
//     btnSomeNone.style.display = 'none';
// };

//

//

// - створити інпут який приймає вік людини та кнопку яка підтверджує дію.При натисканні на кнопку зчитати
//   інформацію з інпуту та перевірити вік чи меньше він ніж 18, та повідомити про це користувача

// const formAge = document.createElement('div');
// const inputAgePerson = document.createElement('input');
// const btnAgePerson = document.createElement('button');

// inputAgePerson.setAttribute('placeholder', 'Ваш вік?');
// btnAgePerson.innerText = 'Перевірка';

// document.body.appendChild(formAge);
// formAge.appendChild(inputAgePerson);
// formAge.appendChild(btnAgePerson);

// btnAgePerson.onclick = () => {
//     const userAge = +inputAgePerson.value;
//     if (userAge >= 18) {
//         alert('Залетай, все гуд!');
//     } else if (userAge < 18) {
//         alert('Ты слишком мал =(');
//     } else {
//         alert('Введіть коректні дані!');
//     }
// };

//

//

// - Создайте меню, которое раскрывается/сворачивается при клике

// const containerMenu = document.createElement('div');
// const blockMenu = document.createElement('div');
// const btnMenu = document.createElement('div');

// const arrMenu = ['Головна', 'Мережа', 'Вакансії', 'Повідомлення', 'Сповіщення', 'Профіль', 'Статистика', 'Розміщення', 'Групи'];

// containerMenu.classList.add('containerMenu');
// blockMenu.classList.add('blockMenu');
// btnMenu.classList.add('btnMenu');

// btnMenu.innerHTML = 'Показати меню';
// arrMenu.forEach((value) => {
//     const itemMenu = document.createElement('div');
//     itemMenu.classList.add('itemMenu');
//     itemMenu.innerText = value;
//     blockMenu.appendChild(itemMenu);
// });

// blockMenu.style.display = 'none';

// containerMenu.appendChild(btnMenu);
// document.body.appendChild(containerMenu);
// document.body.appendChild(blockMenu);

// btnMenu.addEventListener('click', (e) => {
//     e.stopPropagation();
//     if (blockMenu.style.display === 'flex') {
//         btnMenu.innerHTML = 'Показати меню';
//         blockMenu.style.display = 'none';
//     } else if (blockMenu.style.display === 'none') {
//         btnMenu.innerText = 'Сховати меню';
//         blockMenu.style.display = 'flex';
//     }
// });

//

//

// - Создать список комментариев , пример объекта коментария - {title : 'lorem', body:'lorem ipsum dolo sit ameti'}.
// Вывести список комментариев в документ, каждый в своем блоке.
//  Добавьте каждому комментарию по кнопке для сворачивания его body.
// const objComments = [
//     {
//         title: 'Завдання №1',
//         body:
//             'Создать произвольный елемент с id = text.  Используя JavaScript, сделайте так, чтобы при клике на кнопку исчезал элемент с id="text".',
//     },
//     {
//         title: 'Завдання №2',
//         body:
//             'Создайте кнопку, при клике на которую, она будет скрывать сама себя.',
//     },
//     {
//         title: 'Завдання №3',
//         body:
//             'створити інпут який приймає вік людини та кнопку яка підтверджує дію.При натисканні на кнопку зчитати інформацію з інпуту та перевірити вік чи меньше він ніж 18, та повідомити про це користувача.',
//     },
//     {
//         title: 'Завдання №4',
//         body:
//             'Создать список комментариев , пример объекта коментария. Вывести список комментариев в документ, каждый в своем блоке. Добавьте каждому комментарию по кнопке для сворачивания его body.',
//     },
//     {
//         title: 'Завдання №5',
//         body:
//             'створити 2 форми  по 2 інпути в кожній. ствоирити кнопку при кліку на яку считується та виводиться на консоль інформація з цих 2х форм. Кнопка повинна лежати за межами форм (Щоб ьуникнути  перезавантаження сторінки) Доступ до інпутів через Forms API. Отже дайте формі та інпутам всі необхідні атрибути.',
//     },
//     {
//         title: 'Завдання №6',
//         body:
//             'Створити функцію, яка генерує таблицю. Перший аргумент визначає кількість строк. Другий параметр визначає кліькіть ячеєк в кожній строці. Третій параметр визначає елемент в який потрібно таблицю додати.',
//     },
//     {
//         title: 'Завдання №7',
//         body:
//             'Сворити масив не цензцрних слів. Сворити інпут текстового типу. Якщо людина вводить слово і воно міститься в масиві не цензурних слів кинути алерт з попередженням. Перевірку робити при натисканні на кнопку',
//     },
//     {
//         title: 'Завдання №8',
//         body:
//             'Сворити масив не цензцрних слів.Сворити інпут текстового типу. Потрібно перевіряти чи не містить ціле речення в собі погані слова. Кинути алерт з попередженням у випадку якщо містить. Перевірку робити при натисканні на кнопку',
//     },
//     {
//         title: 'Завдання №9',
//         body:
//             'создать скрипт, который берет считывает на странице (rules.html) текст и делает сбоку меню-оглавление по всем заголовкам которые есть в тексте. При клике на пункт оглавления вы должны отправляться к этому пункту в тексте',
//     },
//     {
//         title: 'Завдання №10',
//         body:
//             'Создать три чекбокса. Каждый из них активирует фильтр для вышеуказаного массива. Фильтры могут работать как вместе так и по отдельности. 1й - отфильтровывает пользователей со статусом false (осталяет со статусом false) 2й - оставляет старше 29 лет включительно 3й - оставляет тех у кого город киев. Данные выводить в документ',
//     },
//     {
//         title: 'Завдання №11',
//         body:
//             '(Прям овердоз с рекурсией) Создать функцию которая принимает какой-либо элемент DOM-структуры .Функция создает в боди 2 кнопки (назад/вперед) при нажатии вперед, вы переходите к дочернему элементу, при еще одном нажатии на "вперед", вы переходите к следующему дочернему элементу (лежащему на одном уровне) НО если у (какого-либо)дочеренего элемента есть дети, то нажатие "вперед" позволяет нам войти внутрь элемента и  выводит первого ребенка. и тд. Когда все дети заканчиваются, мы выходим из данного дочернего элемента и переходим к следующему, лежащему с ним на одном уровне',
//     },
//     {
//         title: 'Завдання №12',
//         body:
//             'При виділені сегменту тексту на сторінці він стає жирний/курсивний/або якось іншим способом змінює свій стан',
//     },
// ];
// const containerListComents = document.createElement('div');
// containerListComents.classList.add('containerListComents');
// document.body.appendChild(containerListComents);

// objComments.forEach((value) => {
//     const objectWraper = document.createElement('div');
//     const titleWraper = document.createElement('div');
//     const titleLC = document.createElement('div');
//     const bodyLC = document.createElement('div');
//     const btnLC = document.createElement('button');
//     titleLC.innerText = value.title;
//     bodyLC.innerText = value.body;
//     btnLC.innerText = 'Показати більше';
//     bodyLC.style.display = 'none';

//     btnLC.addEventListener('click', (e) => {
//         e.stopPropagation();

//         if (bodyLC.style.display === 'none') {
//             bodyLC.style.display = 'block';
//             btnLC.innerText = 'Звернути';
//         } else if (bodyLC.style.display === 'block') {
//             bodyLC.style.display = 'none';
//             btnLC.innerText = 'Показати більше';
//         } else {
//             console.log('у нас проблемы, зови админа =)');
//         }
//     });

//     titleWraper.classList.add('titleWraper');
//     objectWraper.classList.add('objectWraper');
//     titleLC.classList.add('titleLC');
//     bodyLC.classList.add('bodyLC');
//     btnLC.classList.add('btnLC');

//     titleWraper.appendChild(titleLC);
//     titleWraper.appendChild(btnLC);
//     objectWraper.appendChild(titleWraper);
//     objectWraper.appendChild(bodyLC);
//     containerListComents.appendChild(objectWraper);
// });

//

//

// - створити 2 форми  по 2 інпути в кожній. ствоирити кнопку при кліку на яку считується та виводиться на консоль
//   інформація з цих 2х форм.
// Кнопка повинна лежати за межами форм (Щоб ьуникнути  перезавантаження сторінки)
// Доступ до інпутів через Forms API. Отже дайте формі та інпутам всі необхідні атрибути.

// const input1 = document.forms.form1.input1;
// const input2 = document.forms.form1.input2;
// const btnForm1 = document.querySelectorAll('.btn')[0];
// const input3 = document.forms.form2.input3;
// const input4 = document.forms.form2.input4;
// const btnForm2 = document.querySelectorAll('.btn')[1];

// btnForm1.addEventListener('click', (e) => {
//     e.stopPropagation();
//     if (input1.value && input2.value) {
//         console.log(`Ім'я - ${input1.value}`);
//         console.log(`Прізвище - ${input2.value}`);
//         input1.value = '';
//         input2.value = '';
//     } else if (!input1.value && !input2.value) {
//         input1.style.border = 'solid 2px red';
//         input1.style.color = 'red';
//         input2.style.border = 'solid 2px red';
//         input2.style.color = 'red';
//     } else if (!input1.value) {
//         input1.style.border = 'solid 2px red';
//         input1.style.color = 'red';
//     } else if (!input2.value) {
//         input2.style.border = 'solid 2px red';
//         input2.style.color = 'red';
//     }
// });
// btnForm2.addEventListener('click', (e) => {
//     e.stopPropagation();
//     if (input3.value && input4.value) {
//         console.log(`Ім'я - ${input3.value}`);
//         console.log(`Прізвище - ${input4.value}`);
//         input3.value = '';
//         input4.value = '';
//     } else if (!input3.value && !input3.value) {
//         input3.style.border = 'solid 2px red';
//         input3.style.color = 'red';
//         input4.style.border = 'solid 2px red';
//         input4.style.color = 'red';
//     } else if (!input3.value) {
//         input3.style.border = 'solid 2px red';
//         input3.style.color = 'red';
//     } else if (!input4.value) {
//         input4.style.border = 'solid 2px red';
//         input4.style.color = 'red';
//     }
// });

//

//

// - Створити функцію, яка генерує таблицю.
// Перший аргумент визначає кількість строк.
// Другий параметр визначає кліькіть ячеєк в кожній строці.
// Третій параметр визначає елемент в який потрібно таблицю додати.

// let numRows = null;
// let numColums = null;
// let tableContent = '';

// const tableConteiner = document.getElementsByClassName('tableConteiner')[0];
// const table = document.createElement('table');
// table.setAttribute('border', '2');
// table.style.display = 'none';
// const createTable = (row, colum, whereAdd, content) => {
//     const caption = document.createElement('caption');
//     const thead = document.createElement('thead');
//     whereAdd.appendChild(table);
//     table.classList.add('table');
//     table.appendChild(caption);
//     caption.innerText = 'MY  TABLE';
//     table.appendChild(thead);

//     for (let i = 0; i <= colum; i++) {
//         const th = document.createElement('th');
//         if (!i) {
//             th.innerText = '';
//         } else {
//             th.innerText = `колонка ${i}`;
//         }
//         table.appendChild(th);
//     }
//     for (let i = 0; i <= row; i++) {
//         const tbody = document.createElement('tbody');

//         for (let j = 0; j <= colum; j++) {
//             const td = document.createElement('td');
//             if (!i) {
//                 continue;
//             } else if (!j) {
//                 td.innerText = `ряд ${i}`;
//             } else {
//                 td.innerText = content;
//             }
//             table.appendChild(td);
//         }
//         table.appendChild(tbody);
//     }
// };

// //

// // - Створити 3 инпута та кнопку. Один визначає кількість рядків, другий - кількість ячеєк, третій вмиіст ячеєк.
// // При натисканні кнопки, вся ця інформація зчитується і формується табличка, з відповідним вмістом.
// // (Додатковачастина для завдання)

// const tableBtn = document.querySelector('.button');
// const inputRow = document.querySelector('#input5');
// const inputColum = document.querySelector('#input6');
// const inputText = document.querySelector('#input7');
// tableBtn.addEventListener('click', (e) => {
//     if (table.style.display === 'none') {
//         e.stopPropagation();
//         table.style.display = 'block';
//         tableBtn.innerText = 'Видалити таблицю';
//         numRows = inputRow.value;
//         numColums = inputColum.value;
//         tableContent = inputText.value;
//         createTable(numRows, numColums, tableConteiner, tableContent);
//         inputRow.value = '';
//         inputColum.value = '';
//         inputText.value = '';
//     } else {
//         table.style.display = 'none';
//         tableBtn.innerText = 'Створити таблицю';
//     }
// });

//

//

// - Напишите «Карусель» – ленту изображений, которую можно листать влево-вправо нажатием на стрелочки.

// const img = document.getElementById('img');
// const back = document.getElementById('back');
// const next = document.getElementById('next');

// const arrImages = [
//    './img/1.jpg',
//    './img/2.jpg',
//    './img/3.jpg',
//    './img/4.jpg',
//    './img/5.jpg',
//    './img/6.jpg',
//    './img/7.jpg',
// ];

// img.src = arrImages[1]

// let i = 1;
// back.addEventListener('click', (e) => {
//    if (i<=1) {
//       back.style.display = 'none';
//     }
//     if (i<5) {
//         next.style.display = 'block';
//     }

//     e.stopPropagation();
//     img.src = arrImages[i];

//     i--;
//     console.log(i);
// });

// next.addEventListener('click', (e) => {
//     if (i>=5) {
//         next.style.display = 'none';
//     }
//     if (i>1) {
//         back.style.display = 'block';
//       }

//       e.stopPropagation();
//       img.src = arrImages[i];
//       i++;
//       console.log(i);
//     });

//

//==============================================================================================
//

// - Сворити масив не цензцрних слів.
const arrBadWords = [
  'архипиздрит',
  'басран',
  'бздение',
  'бздеть',
  'бздех',
  'бзднуть',
  'бздун',
  'бздунья',
  'бздюха',
  'бикса',
  'блежник',
  'блудилище',
  'бляд',
  'блябу',
  'блябуду',
  'блядун',
  'блядунья',
  'блядь',
  'блядюга',
  'взьебка',
  'волосянка',
  'взьебывать',
  'выблядок',
  'выблядыш',
  'выебать',
  'выеть',
  'выпердеть',
  'высраться',
  'выссаться',
  'говенка',
  'говенный',
  'говешка',
  'говназия',
  'говнецо',
  'говно',
  'говноед',
  'говночист',
  'говнюк',
  'говнюха',
  'говнядина',
  'говняк',
  'говняный',
  'говнять',
  'гондон',
  'дермо',
  'долбоеб',
  'дрисня',
  'дрист',
  'дристать',
  'дристануть',
  'дристун',
  'дристуха',
  'дрочена',
  'дрочила',
  'дрочилка',
  'дрочить',
  'дрочка',
  'ебало',
  'ебальник',
  'ебануть',
  'ебаный',
  'ебарь',
  'ебатория',
  'ебать',
  'ебаться',
  'ебец',
  'ебливый',
  'ебля',
  'ебнуть',
  'ебнуться',
  'ебня',
  'ебун',
  'елда',
  'елдак',
  'елдачить',
  'заговнять',
  'задристать',
  'задрока',
  'заеба',
  'заебанец',
  'заебать',
  'заебаться',
  'заебываться',
  'заеть',
  'залупа',
  'залупаться',
  'залупить',
  'залупиться',
  'замудохаться',
  'засерун',
  'засеря',
  'засерать',
  'засирать',
  'засранец',
  'засрун',
  'захуячить',
  'злоебучий',
  'изговнять',
  'изговняться',
  'кляпыжиться',
  'курва',
  'курвенок',
  'курвин',
  'курвяжник',
  'курвяжница',
  'курвяжный',
  'манда',
  'мандавошка',
  'мандей',
  'мандеть',
  'мандища',
  'мандюк',
  'минет',
  'минетчик',
  'минетчица',
  'мокрохвостка',
  'мокрощелка',
  'мудак',
  'муде',
  'мудеть',
  'мудила',
  'мудистый',
  'мудня',
  'мудоеб',
  'мудозвон',
  'муйня',
  'набздеть',
  'наговнять',
  'надристать',
  'надрочить',
  'наебать',
  'наебнуться',
  'наебывать',
  'нассать',
  'нахезать',
  'нахуйник',
  'насцать',
  'обдристаться',
  'обдристаться',
  'обосранец',
  'обосрать',
  'обосцать',
  'обосцаться',
  'обсирать',
  'опизде',
  'отпиздячить',
  'отпороть',
  'отъеть',
  'охуевательский',
  'охуевать',
  'охуевающий',
  'охуеть',
  'охуительный',
  'охуячивать',
  'охуячить',
  'педрик',
  'пердеж',
  'пердение',
  'пердеть',
  'пердильник',
  'перднуть',
  'пердун',
  'пердунец',
  'пердунина',
  'пердунья',
  'пердуха',
  'пердь',
  'передок',
  'пернуть',
  'пидор',
  'пизда',
  'пиздануть',
  'пизденка',
  'пиздеть',
  'пиздить',
  'пиздища',
  'пиздобратия',
  'пиздоватый',
  'пиздорванец',
  'пиздорванка',
  'пиздострадатель',
  'пиздун',
  'пиздюга',
  'пиздюк',
  'пиздячить',
  'писять',
  'питишка',
  'плеха',
  'подговнять',
  'подъебнуться',
  'поебать',
  'поеть',
  'попысать',
  'посрать',
  'поставить',
  'поцоватый',
  'презерватив',
  'проблядь',
  'проебать',
  'промандеть',
  'промудеть',
  'пропиздеть',
  'пропиздячить',
  'пысать',
  'разъеба',
  'разъебай',
  'распиздай',
  'распиздеться',
  'распиздяй',
  'распроеть',
  'растыка',
  'сговнять',
  'секель',
  'серун',
  'серька',
  'сика',
  'сикать',
  'сикель',
  'сирать',
  'сирывать',
  'скурвиться',
  'скуреха',
  'скурея',
  'скуряга',
  'скуряжничать',
  'спиздить',
  'срака',
  'сраный',
  'сранье',
  'срать',
  'срун',
  'ссака',
  'ссаки',
  'ссать',
  'старпер',
  'струк',
  'суходрочка',
  'сцавинье',
  'сцака',
  'сцаки',
  'сцание',
  'сцать',
  'сциха',
  'сцуль',
  'сцыха',
  'сыкун',
  'титечка',
  'титечный',
  'титка',
  'титочка',
  'титька',
  'трипер',
  'триппер',
  'уеть',
  'усраться',
  'усцаться',
  'фик',
  'фуй',
  'хезать',
  'хер',
  'херня',
  'херовина',
  'херовый',
  'хитрожопый',
  'хлюха',
  'хуевина',
  'хуевый',
  'хуек',
  'хуепромышленник',
  'хуерик',
  'хуесос',
  'хуище',
  'хуй',
  'хуйня',
  'хуйрик',
  'хуякать',
  'хуякнуть',
  'целка',
  'шлюха',
];
// Сворити інпут текстового типу.
// Якщо людина вводить слово і воно міститься в масиві не цензурних слів
// кинути алерт з попередженням.
// Перевірку робити при натисканні на кнопку

// const inputTask10 = document.createElement('input');
// const btnTask10 = document.createElement('button');
// inputTask10.classList.add('input');
// btnTask10.innerText = 'перевірити текст';

// document.body.appendChild(inputTask10);
// document.body.appendChild(btnTask10);

// btnTask10.addEventListener('click', (e) => {
//    const arrMyWords = inputTask10.value.toLowerCase().split(' ');
//    const arr = [];
//    arrMyWords.find((value) => {
//       for (const word of arrBadWords) {
//          if (value === word) {
//             alert('Мамка по попці дасть, гівнюк!');
//             arr.push(value);
//             break;
//          }
//       }
//    });
//    !arr.length, console.log('Дані прийнято');
// });

//

//

// - Сворити масив не цензцрних слів.
// Сворити інпут текстового типу.
// Потрібно перевіряти чи не містить ціле речення в собі погані слова.
// Кинути алерт з попередженням у випадку якщо містить.
// Перевірку робити при натисканні на кнопку

// const inputTask10 = document.createElement('input');
// const btnTask10 = document.createElement('button');
// inputTask10.classList.add('input');
// btnTask10.innerText = 'перевірити текст';

// document.body.appendChild(inputTask10);
// document.body.appendChild(btnTask10);

// btnTask10.addEventListener('click', (e) => {
//   e.stopPropagation();
//   const arrMyWords = inputTask10.value.toLowerCase().split(' ');

//   arrMyWords.map((value) => {
//     value.includes(',');
//   });

//   let haveBadWord = arrMyWords.some((value) => {
//     for (const word of arrBadWords) {
//       if (value === word) {
//         return true;
//       }
//     }
//   });

//   if (haveBadWord) {
//     alert('Мамка по попці дасть, гівнюк!');
//     return;
//   } else if (inputTask10.value) {
//     console.log('Дані прийнято');
//     return;
//   }
//   console.log('Ви не ввели дані');
// });

//

//

// -- создать скрипт, который берет считывает на странице (rules.html) текст и делает сбоку меню-оглавление
//  по всем заголовкам которые есть в тексте.
// При клике на пункт оглавления вы должны отправляться к этому пункту в тексте

// const wraper = document.querySelector('#wrap');
// const nameRules = wraper.querySelectorAll('h2');
// const ul = document.createElement('ul');

// ul.innerText = 'Зміст';
// wraper.prepend(ul);

// nameRules.forEach((value, index) => {
//   const li = document.createElement('li');
//   const a = document.createElement('a');

//   a.innerHTML = value.textContent;
//   a.setAttribute('href', `#${index + 1}`);
//   nameRules[index].setAttribute('id', index + 1);

//   li.appendChild(a);
//   ul.appendChild(li);
// });

//

//

// -- взять массив пользователей
let usersWithAddress = [
  {
    id: 1,
    name: 'vasya',
    age: 31,
    status: false,
    address: { city: 'Lviv', street: 'Shevchenko', number: 16 },
  },
  {
    id: 2,
    name: 'petya',
    age: 30,
    status: true,
    address: { city: 'Kyiv', street: 'Shevchenko', number: 1 },
  },
  {
    id: 3,
    name: 'kolya',
    age: 29,
    status: true,
    address: { city: 'Lviv', street: 'Shevchenko', number: 121 },
  },
  {
    id: 4,
    name: 'olya',
    age: 28,
    status: false,
    address: { city: 'Ternopil', street: 'Shevchenko', number: 90 },
  },
  {
    id: 5,
    name: 'max',
    age: 30,
    status: true,
    address: { city: 'Lviv', street: 'Shevchenko', number: 115 },
  },
  {
    id: 6,
    name: 'anya',
    age: 31,
    status: false,
    address: { city: 'Kyiv', street: 'Shevchenko', number: 2 },
  },
  {
    id: 7,
    name: 'oleg',
    age: 28,
    status: false,
    address: { city: 'Ternopil', street: 'Shevchenko', number: 22 },
  },
  {
    id: 8,
    name: 'andrey',
    age: 29,
    status: true,
    address: { city: 'Lviv', street: 'Shevchenko', number: 43 },
  },
  {
    id: 9,
    name: 'masha',
    age: 30,
    status: true,
    address: { city: 'Kyiv', street: 'Shevchenko', number: 12 },
  },
  {
    id: 10,
    name: 'olya',
    age: 31,
    status: false,
    address: { city: 'Lviv', street: 'Shevchenko', number: 16 },
  },
  {
    id: 11,
    name: 'max',
    age: 31,
    status: true,
    address: { city: 'Ternopil', street: 'Shevchenko', number: 121 },
  },
];
// Создать три чекбокса. Каждый из них активирует фильтр для вышеуказаного массива. Фильтры могут работать как вместе
//  так и по отдельности.
// 1й - отфильтровывает пользователей со статусом false (осталяет со статусом false)
// 2й - оставляет старше 29 лет включительно
// 3й - оставляет тех у кого город киев
// Данные выводить в документ

const content = document.getElementById('content');
const box1 = document.getElementById('box1');
const box2 = document.getElementById('box2');
const box3 = document.getElementById('box3');
const btn = document.getElementById('btnFilter');

btn.addEventListener('click', () => {
  let newArrUsers = JSON.parse(JSON.stringify(usersWithAddress));

  if (box1.checked) {newArrUsers = newArrUsers.filter((value) => value.status);}
  if (box2.checked) {newArrUsers = newArrUsers.filter((value) => value.age > 29);}
  if (box3.checked) {newArrUsers = newArrUsers.filter((value) => value.address.city === 'Kyiv');}
console.log(box2);


  for (const i of newArrUsers) {
    let {id, name, age, status, address: { city, street, number}} = i;

    const div = document.createElement('div');
    const p0 = document.createElement('p');
    const p1 = document.createElement('p');
    const p2 = document.createElement('p');
    const p3 = document.createElement('p');
    const p4 = document.createElement('p');
    const p5 = document.createElement('p');
    const p6 = document.createElement('p');

    p0.innerHTML = `Ім'я : <b>${name}</b>`;
    p1.innerHTML = `ID : <b>${id}</b>`;
    p2.innerHTML = `Вік : <b>${age}</b>`;
    p3.innerHTML = `Статус : <b>${status}</b>`;
    p4.innerHTML = `Місто : <b>${city}</b>`;
    p5.innerHTML = `Вулиця : <b>${street}</b>`;
    p6.innerHTML = `Номер будинку : <b>${number}</b>`;

    div.appendChild(p0);
    div.appendChild(p1);
    div.appendChild(p2);
    div.appendChild(p3);
    div.appendChild(p4);
    div.appendChild(p5);
    div.appendChild(p6);

    div.classList.add('card');
    content.appendChild(div);
  }
});

//

// *****(Прям овердоз с рекурсией) Создать функцию которая принимает какой-либо элемент DOM-структуры .Функция создает
// в боди 2 кнопки (назад/вперед) при нажатии вперед, вы переходите к дочернему элементу, при еще одном нажатии
// на "вперед", вы переходите к следующему дочернему элементу (лежащему на одном уровне)НО если у (какого-либо)
// дочеренего элемента есть дети, то нажатие "вперед" позволяет нам войти внутрь элемента и  выводит первого ребенка.
// и тд. Когда все дети заканчиваются, мы выходим из данного дочернего элемента и переходим к следующему, лежащему с
// ним на одном уровне

// *** При виділені сегменту тексту на сторінці він стає жирний/курсивний/або якось іншим способом змінює свій стан
