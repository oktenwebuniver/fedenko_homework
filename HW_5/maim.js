//                   Task #1
// ==============================================
// -  Створити функцію конструктор для об'єкту який описує теги
// Властивості
//  -назва тегу
//  - опис його дій
//  - масив з атрибутами (2-3 атрибути максимум)
//  Кожен атрибут описати як окремий який буде містити
//  -назву атрибуту
//  -опис дії атрибуту
//  інформацію брати з htmlbook.ru

//  Таким чином описати теги
//  -a
//  -div
//  -h1
//  -span
//  -input
//  -form
//  -option
//  -select
//  Приклад результату
//    {
//         titleOfTag: 'area',
//         action: `Каждый элемент <area> определяет активные области изображения, которые являются ссылками...`,
//         attrs: [
//         {titleOfAttr: 'accesskey', actionOfAttr: 'Переход к области с помощью комбинации клавиш'},
//         {/*some props and values*/},
//         {/*...*/},
//         {/*...*/},
//         ]

//    }
// ==============================================

// function DescriptionTag (titleOfTag, action, attrs) {
//       this.titleOfTag = titleOfTag;
//       this.action = action;
//       this.attrs = attrs;

//       this.output = function () {
//           console.log(this);
//       }
//    }

// let a = new DescriptionTag('a', 'Устанавливает ссылку или якорь', [
//    { accesskey: 'Активация ссылки с помощью комбинации клавиш' },
//    { coords: 'Устанавливает координаты активной области' },
//    { download: 'Предлагает скачать указанный по ссылке файл' },
// ]);
// let div = new DescriptionTag(
//    'div',
//    'Выделения фрагмента документа с целью изменения вида содержимого',
//    [
//       { align: 'Задает выравнивание содержимого тега' },
//       { title: 'Добавляет всплывающую подсказку к содержимому' },
//    ],
// );
// let h1 = new DescriptionTag(
//    'h1',
//    'Предлагает шесть заголовков разного уровня',
//    [{ align: 'Определяет выравнивание заголовка' }],
// );
// let span = new DescriptionTag(
//    'span',
//    'Определения строчных элементов документа',
//    ['Для этого тега доступны универсальные атрибуты'],
// );
// let input = new DescriptionTag(
//    'input',
//    'Предназначен для создания текстовых полей, различных кнопок',
//    [
//       { accesskey: 'Переход к элементу с помощью комбинации клавиш' },
//       { align: 'Определяет выравнивание изображения' },
//       { formaction: 'Определяет адрес обработчика формы' },
//    ],
// );
// let form = new DescriptionTag('form', 'Устанавливает форму на веб-странице', [
//    {
//       action:
//          'Адрес программы или документа, который обрабатывает данные формы',
//    },
//    { autocomplete: 'Включает автозаполнение полей формы' },
//    {
//       novalidate:
//          'Отменяет встроенную проверку данных формы на корректность ввода',
//    },
// ]);
// let option = new DescriptionTag(
//    'option',
//    'Определяет отдельные пункты списка, создаваемого с помощью контейнера',
//    [
//       { disabled: 'Заблокировать для доступа элемент списка' },
//       { label: 'Указание метки пункта списка' },
//       {
//          selected: 'Заранее устанавливает определенный пункт списка выделенным',
//       },
//    ],
// );
// let select = new DescriptionTag(
//    'select',
//    'создать элемент интерфейса в виде раскрывающегося списка',
//    [
//       {
//          accesskey:
//             'Позволяет перейти к списку с помощью некоторого сочетания клавиш',
//       },
//       {
//          autofocus:
//             'Устанавливает, что список получает фокус после загрузки страницы',
//       },
//       {
//          multiple:
//             'Позволяет одновременно выбирать сразу несколько элементов списка',
//       },
//    ],
// );

// a.output();
// div.output();
// span.output();
// input.output();
// form.output();
// option.output();
// select.output();

//

//                   Task #2
// ==============================================
// -  Створити класс  для об'єкту який описує теги
// Властивості
//  -назва тегу
//  - опис його дій
//  - масив з атрибутами (2-3 атрибути максимум)
//  Кожен атрибут описати як окремий який буде містити
//  -назву атрибуту
//  -опис дії атрибуту
//  інформацію брати з htmlbook.ru

//  Таким чином описати теги
//  -a
//  -div
//  -h1
//  -span
//  -input
//  -form
//  -option
//  -select
//  Приклад результату
//    {
//         titleOfTag: 'area',
//         action: `Каждый элемент <area> определяет активные области изображения, которые являются ссылками...`,
//         attrs: [
//         {titleOfAttr: 'accesskey', actionOfAttr: 'Переход к области с помощью комбинации клавиш'},
//         {/*some props and values*/},
//         {/*...*/},
//         {/*...*/},
//         ]

//    }
// ==============================================

// class DescriptionTag {
//    constructor(titleOfTag, action, attrs) {
//       this.titleOfTag = titleOfTag;
//       this.action = action;
//       this.attrs = attrs;
//    }
//    output() {
//        console.log(this);
//    }
// }

// let a = new DescriptionTag('a', 'Устанавливает ссылку или якорь', [
//    { accesskey: 'Активация ссылки с помощью комбинации клавиш' },
//    { coords: 'Устанавливает координаты активной области' },
//    { download: 'Предлагает скачать указанный по ссылке файл' },
// ]);
// let div = new DescriptionTag(
//    'div',
//    'Выделения фрагмента документа с целью изменения вида содержимого',
//    [
//       { align: 'Задает выравнивание содержимого тега' },
//       { title: 'Добавляет всплывающую подсказку к содержимому' },
//    ],
// );
// let h1 = new DescriptionTag(
//    'h1',
//    'Предлагает шесть заголовков разного уровня',
//    [{ align: 'Определяет выравнивание заголовка' }],
// );
// let span = new DescriptionTag(
//    'span',
//    'Определения строчных элементов документа',
//    ['Для этого тега доступны универсальные атрибуты'],
// );
// let input = new DescriptionTag(
//    'input',
//    'Предназначен для создания текстовых полей, различных кнопок',
//    [
//       { accesskey: 'Переход к элементу с помощью комбинации клавиш' },
//       { align: 'Определяет выравнивание изображения' },
//       { formaction: 'Определяет адрес обработчика формы' },
//    ],
// );
// let form = new DescriptionTag('form', 'Устанавливает форму на веб-странице', [
//    {
//       action:
//          'Адрес программы или документа, который обрабатывает данные формы',
//    },
//    { autocomplete: 'Включает автозаполнение полей формы' },
//    {
//       novalidate:
//          'Отменяет встроенную проверку данных формы на корректность ввода',
//    },
// ]);
// let option = new DescriptionTag(
//    'option',
//    'Определяет отдельные пункты списка, создаваемого с помощью контейнера',
//    [
//       { disabled: 'Заблокировать для доступа элемент списка' },
//       { label: 'Указание метки пункта списка' },
//       {
//          selected: 'Заранее устанавливает определенный пункт списка выделенным',
//       },
//    ],
// );
// let select = new DescriptionTag(
//    'select',
//    'создать элемент интерфейса в виде раскрывающегося списка',
//    [
//       {
//          accesskey:
//             'Позволяет перейти к списку с помощью некоторого сочетания клавиш',
//       },
//       {
//          autofocus:
//             'Устанавливает, что список получает фокус после загрузки страницы',
//       },
//       {
//          multiple:
//             'Позволяет одновременно выбирать сразу несколько элементов списка',
//       },
//    ],
// );
// a.output();
// div.output();
// span.output();
// input.output();
// form.output();
// option.output();
// select.output();

//

//                   Task #3
// ==============================================
// - Створити об'єкт car, з властивостями модель, виробник, рік випуску, максимальна швидкість, об'єм двигуна. додати в
//    об'єкт функції:
// -- drive () - яка виводить в консоль "їдемо зі швидкістю {максимальна швидкість} на годину"
// -- info () - яка виводить всю інформацію про автомобіль
// -- increaseMaxSpeed (newSpeed) - яка підвищує значення максимальної швидкості на значення newSpeed
// -- changeYear (newValue) - змінює рік випуску на значення newValue
// -- addDriver (driver) - приймає об'єкт який "водій" з довільним набором полів, і доавляет його в поточний об'єкт car
// ==============================================

// class Car {
//    constructor(model, madeCountry, madeYear, maxSpeed, color) {
//       this.model = model;
//       this.madeCountry = madeCountry;
//       this.madeYear = madeYear;
//       this.maxSpeed = maxSpeed;
//       this.color = color;
//       this.driver = false;
//    }
//    drive() {
//       console.log(`Їдемо зі швидкістю ${this.maxSpeed} на годину`);
//    }
//    info() {
//       console.log(this);
//    }
//    increaseMaxSpeed(newSpeed) {
//       this.maxSpeed += newSpeed;
//       console.log(this.maxSpeed);
//    }
//    changeYear(newValue) {
//       this.madeYear = newValue;
//       console.log(this.madeYear);
//    }
//    addDriver(newDriver) {
//       this.driver = newDriver;
//       console.log(this);
//    }
// }

// let tesla = new Car('tesla:', 'Ukrain', 2020, 355, 'pink');
// let vaz = new Car('vaz', 'Germany', 2010, 212, 'red');
// let audi = new Car('audi', 'USA', 2012, 263, 'white');
// let niva = new Car('niva', 'Russia', 2005, 200, 'blue');
// let opel = new Car('opel', 'Italia', 2015, 287, 'yellow');
// let zaz = new Car('zaz', 'Ukrain', 2010, 154, 'black');
// let mitsubisi = new Car('mitsubisi', 'UK', 2015, 312, 'yellow');
// let alfraomeo = new Car('alfraomeo', 'Italia', 2018, 385, 'broun');
// let toyota = new Car('toyota', 'Ukrain', 2003, 208, 'red');

// let driverMax = {
//    name: 'Max',
//    age: 32,
//    gender: 'man',
//    experience: true,
// };

// tesla.drive();
// vaz.info();
// audi.increaseMaxSpeed(50);
// niva.changeYear(1980);
// alfraomeo.addDriver(driverMax);

//

//                   Task #4
// ==============================================
// - Створити функцію конструктор яка дозволяє створювати об'єкти car, з властивостями модель, виробник, рік
//    випуску, максимальна швидкість, об'єм двигуна. додати в об'єкт функції:
// -- drive () - яка виводить в консоль "їдемо зі швидкістю {максимальна швидкість} на годину"
// -- info () - яка виводить всю інформацію про автомобіль
// -- increaseMaxSpeed (newSpeed) - яка підвищує значення максимальної швидкості на значення newSpeed
// -- changeYear (newValue) - змінює рік випуску на значення newValue
// -- addDriver (driver) - приймає об'єкт який "водій" з довільним набором полів, і доавляет його в поточний об'єкт car
// ==============================================

// function Car(model, madeCountry, madeYear, maxSpeed, color) {
//    this.model = model;
//    this.madeCountry = madeCountry;
//    this.madeYear = madeYear;
//    this.maxSpeed = maxSpeed;
//    this.color = color;
//    this.driver = false;

//    this.drive = function () {
//       console.log(`їдемо зі швидкістю ${this.maxSpeed} на годину`);
//    };
//    this.info = function () {
//       console.log(`Модель: ${this.model}`);
//       console.log(`Країна виробник: ${this.madeCountry}`);
//       console.log(`Рік випуску: ${this.madeYear}`);
//       console.log(`Максимальна швидкість: ${this.maxSpeed}`);
//       console.log(`Колір: ${this.color}`);
//    };
//    this.increaseMaxSpeed = function (newSpeed) {
//       this.maxSpeed += newSpeed;
//       console.log(
//          `Після апгрейду максимальна швидність складає ${this.maxSpeed} км/год!`,
//       );
//    };
//    this.changeYear = function (newValue) {
//       this.madeYear = newValue;
//       console.log(this.madeYear);
//    };
//    this.addDriver = function (newDriver) {
//       this.driver = newDriver;
//       console.log(this);
//    };
// }

// let tesla = new Car('tesla:', 'Ukrain', 2020, 355, 'pink');
// let vaz = new Car('vaz', 'Germany', 2010, 212, 'red');
// let audi = new Car('audi', 'USA', 2012, 263, 'white');
// let niva = new Car('niva', 'Russia', 2005, 200, 'blue');
// let opel = new Car('opel', 'Italia', 2015, 287, 'yellow');
// let zaz = new Car('zaz', 'Ukrain', 2010, 154, 'black');
// let mitsubisi = new Car('mitsubisi', 'UK', 2015, 312, 'yellow');
// let alfraomeo = new Car('alfraomeo', 'Italia', 2018, 385, 'broun');
// let toyota = new Car('toyota', 'Ukrain', 2003, 208, 'red');

// let driverMax = {
//    name: 'Max',
//    age: 32,
//    gender: 'man',
//    experience: true,
// };

// tesla.drive();
// audi.info();
// zaz.increaseMaxSpeed(45);
// toyota.changeYear(2015);
// mitsubisi.addDriver(driverMax);

//

//                   Task #5
// ==============================================
// - Створити клас який дозволяє створювати об'єкти car, з властивостями модель, виробник, рік випуску,
//   максимальна швидкість, об'єм двигуна. додати в об'єкт функції:
// -- drive () - яка виводить в консоль "їдемо зі швидкістю {максимальна швидкість} на годину"
// -- info () - яка виводить всю інформацію про автомобіль
// -- increaseMaxSpeed (newSpeed) - яка підвищує значення максимальної швидкості на значення newSpeed
// -- changeYear (newValue) - змінює рік випуску на значення newValue
// -- addDriver (driver) - приймає об'єкт який "водій" з довільним набором полів, і доавляет його в поточний об'єкт car
// ==============================================

// class Car {
//    constructor(model, madeCountry, madeYear, maxSpeed, color) {
//       this.model = model;
//       this.madeCountry = madeCountry;
//       this.madeYear = madeYear;
//       this.maxSpeed = maxSpeed;
//       this.color = color;
//       this.driver = false;
//    }
//    drive() {
//       console.log(`їдемо зі швидкістю ${this.maxSpeed} на годину`);
//    }
//    info() {
//       console.log(`Модель: ${this.model}`);
//       console.log(`Країна виробник: ${this.madeCountry}`);
//       console.log(`Рік випуску: ${this.madeYear}`);
//       console.log(`Максимальна швидкість: ${this.maxSpeed}`);
//       console.log(`Колір: ${this.color}`);
//    }
//    increaseMaxSpeed (newSpeed) {
//     this.maxSpeed +=newSpeed;
//     console.log(`Після апгрейду максимальна швидність складає ${this.maxSpeed} км/год!`);
//    }
//    changeYear (newValue) {
//        this.madeYear = newValue;
//        console.log(this.madeYear);
//    }
//    addDriver (newDriver) {
//     this.driver = newDriver;
//     console.log(this);
//    }
// }

// let tesla = new Car('tesla:', 'Ukrain', 2020, 355, 'pink');
// let vaz = new Car('vaz', 'Germany', 2010, 212, 'red');
// let audi = new Car('audi', 'USA', 2012, 263, 'white');
// let niva = new Car('niva', 'Russia', 2005, 200, 'blue');
// let opel = new Car('opel', 'Italia', 2015, 287, 'yellow');
// let zaz = new Car('zaz', 'Ukrain', 2010, 154, 'black');
// let mitsubisi = new Car('mitsubisi', 'UK', 2015, 312, 'yellow');
// let alfraomeo = new Car('alfraomeo', 'Italia', 2018, 385, 'broun');
// let toyota = new Car('toyota', 'Ukrain', 2003, 208, 'red');

// let driverMax = {
//    name: 'Max',
//    age: 32,
//    gender: 'man',
//    experience: true,
// };

// tesla.drive();
// audi.info();
// zaz.increaseMaxSpeed(45);
// toyota.changeYear(2015);
// mitsubisi.addDriver(driverMax);

//

//                   Task #6
// ==============================================
// -створити класс попелюшка з полями ім'я, вік, розмір ноги
// --Створити 10 попелюшок , покласти їх в масив
// --Сторити об'єкт класу "принц" за допомоги класу який має поля ім'я, вік, туфелька яку він знайшов.
// -- за допоиоги циклу знайти яка попелюшка повинна бути з принцом
// ==============================================

// class Human {
//    constructor(name, age) {
//       this.name = name;
//       this.age = age;
//    }
// }

// class Popelushka extends Human {
//    constructor(name, age, footSize) {
//       super(name, age);
//       this.footSize = footSize;
//    }
// }

// class Princ extends Human {
//    constructor(name, age, bootSize) {
//       super(name, age), (this.bootSize = bootSize);
//    }
//    pickGirl(girls) {
//       for (const girl of girls) {
//          if (this.bootSize === girl.footSize) {
//             console.log(
//                `Принце дістається попелюшка ${girl.name}, ${girl.age} років!`,
//             );
//          }
//       }
//    }
// }

// const princ = new Princ('Alfred', 28, 35);

// const popel1 = new Popelushka('Катерина', 44, 37);
// const popel2 = new Popelushka('Надія', 35, 34);
// const popel3 = new Popelushka('Вікторія', 15, 34);
// const popel4 = new Popelushka('Софія', 25, 36);
// const popel5 = new Popelushka('Людмила', 17, 40);
// const popel6 = new Popelushka('Тамара', 12, 35);
// const popel7 = new Popelushka('Аліна', 15, 36);
// const popel8 = new Popelushka('Ангеліна', 12, 42);
// const popel9 = new Popelushka('Олена', 21, 41);
// const popel10 = new Popelushka('Анна', 20, 38);
// const popel11 = new Popelushka('Оксана', 14, 36);

// const popelushki = [
//    popel1,
//    popel2,
//    popel3,
//    popel4,
//    popel5,
//    popel6,
//    popel7,
//    popel8,
//    popel9,
//    popel10,
//    popel11,
// ];
// princ.pickGirl(popelushki);

//

//                   Task #7
// ==============================================
// -створити функцію конструктор попелюшка з полями ім'я, вік, розмір ноги
// --Створити 10 попелюшок , покласти їх в масив
// --Сторити об'єкт типу "принц" за допомоги функції конструктора з полями ім'я, вік, туфелька яку він
//    знайшов, та функцію "пошук попелюшки"
// -- функція повинна приймати масив попелюшок, та шукає ту котра йому підходить
// ==============================================

function Human(name, age) {
   this.name = name;
   this.age = age;
}

function Popelushka(footSize) {
   super(name, age);
   this.footSize = footSize;
}

function Princ(bootSize) {
   super(name, age);
   this.bootSize = bootSize;

   this.fiendPrincess(listPopelushok) {
       
   };
}

const popel1 = new Popelushka('Катерина', 44, 37);
const popel2 = new Popelushka('Надія', 35, 34);
const popel3 = new Popelushka('Вікторія', 15, 34);
const popel4 = new Popelushka('Софія', 25, 36);
const popel5 = new Popelushka('Людмила', 17, 40);
const popel6 = new Popelushka('Тамара', 12, 35);
const popel7 = new Popelushka('Аліна', 15, 36);
const popel8 = new Popelushka('Ангеліна', 12, 42);
const popel9 = new Popelushka('Олена', 21, 41);
const popel10 = new Popelushka('Анна', 20, 38);
const popel11 = new Popelushka('Оксана', 14, 36);

const listPopelushok = [
   popel1,
   popel2,
   popel3,
   popel4,
   popel5,
   popel6,
   popel7,
   popel8,
   popel9,
   popel10,
   popel11,
];
