'use strict';
// - присвоить каждому из следующих значений свою переменную: 'hello','owu','com'
// 'ua', 1, 10, -999, 123, 3.14, 2.7, 16, true, false Вывести каждую при помощи
// console.log , alert, document.write

let sayHello = 'hello';
let schoolPrograming = 'owu';
let domen = 'com';
let hosting = 'ua';
let num1 = 1;
let num10 = 10;
let num999 = -999;
let num123 = 123;
let numP = 3.14;
let num2 = 2.7;
let num16 = 16;

// console.log(sayHello);
// console.log(schoolPrograming);
// console.log(domen);
// console.log(hosting);
// console.log(num1);
// console.log(num10);
// console.log(num999);
// console.log(num123);
// console.log(numP);
// console.log(num2);
// console.log(num16);

// alert(sayHello);
// alert(schoolPrograming);
// alert(domen);
// alert(hosting);
// alert(num1);
// alert(num10);
// alert(num999);
// alert(num123);
// alert(numP);
// alert(num2);
// alert(num16);

// document.write(sayHello);
// document.write(domen);
// document.write(schoolPrograming);
// document.write(num1);
// document.write(hosting);
// document.write(num999);
// document.write(num10);
// document.write(numP);
// document.write(num123);
// document.write(num16);
// document.write(num2);

// - переопределить каждую переменную из задания 1 дав им произвольные значения Вывести каждую при помощи
// console.log , alert, document.write

sayHello = 'Hi';
schoolPrograming = 'schoolNoNane';
domen = false;
hosting = 'ru';
num1 = true;
num10 = 12;
num999 = 99;
num123 = 321;
numP = 0;
num2 = 16;
num16 = 2;

// console.log(sayHello);
// console.log(schoolPrograming);
// console.log(domen);
// console.log(hosting);
// console.log(num1);
// console.log(num10);
// console.log(num999);
// console.log(num123);
// console.log(numP);
// console.log(num2);
// console.log(num16);

// alert(sayHello);
// alert(schoolPrograming);
// alert(domen);
// alert(hosting);
// alert(num1);
// alert(num10);
// alert(num999);
// alert(num123);
// alert(numP);
// alert(num2);
// alert(num16);

// document.write(sayHello);
// document.write(domen);
// document.write(schoolPrograming);
// document.write(num1);
// document.write(hosting);
// document.write(num999);
// document.write(num10);
// document.write(numP);
// document.write(num123);
// document.write(num16);
// document.write(num2);

// - Создать 3 числовых и 3 стринговых константы. Вывести каждую при помощи console.log , alert, document.write

const pi = 3.14;
const gravAcceler = 9.81;
const numAvogadro = 6.022;
const login = 'admin';
const pass = '12345';
const mail = 'feden@gmail.com';

// console.log(pi);
// console.log(gravAcceler);
// console.log(numAvogadro);
// console.log(login);
// console.log(pass);
// console.log(mail);

// alert(pi);
// alert(gravAcceler);
// alert(numAvogadro);
// alert(login);
// alert(pass);
// alert(mail);

// document.write(pi);
// document.write(gravAcceler);
// document.write(numAvogadro);
// document.write(login);
// document.write(pass);
// document.write(mail);

// - при помощи 3х разных prompt() получить 3 слова которые являются вашими ФИО. Для фамилии имени и отчества
// создать разные переменные. Вывести каждую при помощи console.log , alert, document.write

// let firstName = prompt("Запишіть Ваше ім'я");
// let otchestvo = prompt('Запишіть Ваше побатькові');
// let lastName = prompt('Запишіть Ваше прізвище');

// console.log(firstName);
// console.log(lastName);
// console.log(otchestvo);

// alert(firstName);
// alert(lastName);
// alert(otchestvo);

// document.write(firstName);
// document.write(lastName);
// document.write(otchestvo);

// - Взять переменные из задания 4 и сконкатенировать их в одной переменной person.

// let person = `${lastName} ${firstName} ${otchestvo}`;
// console.log(person);

// - Взять задние 4 и 5 и применить его к ФИО всех членов своей семьи.

// let fatherLastName = prompt('Яке прізвище у вашого батька?');
// let fatherFirstName = prompt("Яке ім'я у вашого батька?");
// let fatherOtchestvo = prompt('Як по-батькові вашого батька?');

// let fatherPerson = `${fatherLastName} ${fatherFirstName} ${fatherOtchestvo}`;
// console.log(fatherPerson);

// let motherLastName = prompt('Яке прізвище у вашої матері?');
// let motherFirstName = prompt("Яке ім'я у вашої матері?");
// let motherOtchestvo = prompt('Яке по-батькові вашої матері?');

// let motherPerson = `${motherLastName} ${motherFirstName} ${motherOtchestvo}`;
// console.log(motherPerson);

// let brotherLastName = prompt('Яке прізвище у вашого батька?');
// let brotherFirstName = prompt("Яке ім'я у вашого батька?");
// let brotherOtchestvo = prompt('Як по-батькові вашого батька?');

// let brotherPerson = `${brotherLastName} ${brotherFirstName} ${brotherOtchestvo}`;
// console.log(brotherPerson);

// - при помощи prompt() получить 3 числа. Привести их к числовому типу при помощи +. вывести их в консоль.

// let case1 = +prompt('введіль своє улюблене число');
// let case2 = +prompt('введіть день свого народження');
// let case3 = +prompt('скільки голів у Змія Горинича?');

// console.log(case1);
// console.log(case2);
// console.log(case3);

// - при помощи prompt() получить 4 числа. Привести их к числовому типу при помощи parseInt. Сложить их между собой
// записав результат в переменную result и вывести в консоль браузера

// let number1 = parseInt(prompt('Введіть число №1'));
// let number2 = parseInt(prompt('Введіть число №2'));
// let number3 = parseInt(prompt('Введіть число №3'));
// let number4 = parseInt(prompt('Введіть число №4'));

// let result = number1 + number2 + number3 + number4;
// console.log(result);

// - при помощи prompt()  получить 3 числа с плавающей точекой. при помощи parseFloat привести их к соответсвующему
// типу. Сложить их между собой записав результат в переменную result и вывести в консоль браузера

// let number11 = parseFloat(prompt('Введіть число №1'));
// let number21 = parseFloat(prompt('Введіть число №2'));
// let number31 = parseFloat(prompt('Введіть число №3'));
// let number41 = parseFloat(prompt('Введіть число №4'));

// let result2 = number11 + number21 + number31 + number41;
// console.log(result2);

// - при помощи prompt()  получить 3 числа с плавающей точекой. Округлить их при помощи Math.round Сложить их между
// собой записав результат в переменную  и вывести в консоль браузера

// let number12 = Math.round(parseFloat(prompt('Введіть число №1')));
// let number22 = Math.round(parseFloat(prompt('Введіть число №2')));
// let number32 = Math.round(parseFloat(prompt('Введіть число №3')));
// let number42 = Math.round(parseFloat(prompt('Введіть число №4')));

// let result3 = number12 + number22 + number32 + number42;
// console.log(result3);

// - при помощи prompt()  получить 2 целых числа. Привести их к целочисленному типу. Первое число - это число
// которое будут возводить в степень. Второе число - это число которое является степенью. При помощи Math.pow
// возвести первое число в степень второго числа.

// let number13 = Math.round(parseInt(prompt('Введіть число №1')));
// let number23 = Math.round(parseInt(prompt('Введіть число №2')));

// let result4 = Math.pow(number13, number23);
// console.log(result4);

// - При помощи оператора опредеоения типа typeof определить типы следующих переменных и вывести их в консоль
// let a = 100; let b = '100'; let c = true; let d = undefined;

// let a = 100;
// let b = '100';
// let c = true;
// let d = undefined;

// console.log(typeof a);
// console.log(typeof b);
// console.log(typeof c);
// console.log(typeof d);

// - Поставьте соответствующий оператор в выражениях что бы получился соответсвующий результат.

// console.log('5 < 6 ->', 5 < 6);
// console.log('5 < 6 ->', 5 > 6);
// console.log('5 < 6 ->', 5 >= 6);
// console.log('5 < 6 ->', 5 != 6);
// console.log('10 >= 10 ->', 10 >= 10);
// console.log('10 <= 10 ->', 10 <= 10);
// console.log('10 != 10 ->', 10 != 10);
// console.log('10 < 10 ->', 10 < 10);
// console.log('10 > 10 ->', 10 > 10);
// console.log("123 === '123' ->", 123 === '123');
// console.log("123 == '123' ->", 123 == '123');






// Дополнительно:
// - Посмотрев на следюующие выражения, скажите, каков будет вывод в консоль
// console.log(132 > 100 && 45 < 12 );
// console.log(34 > 33 && 23 < 90 );
// console.log(99 > 100 && 45 > 12 );
// console.log(132 > 100 || 45 < 12 );
// console.log(111 > 11 || 45 < 111 );
// console.log((111 > 11 || 45 < 111) && (132 > 100 || 45 < 12) );
// console.log((111 > 11 || 45 < 56) || (132 > 100 || 45 < 12) );
// console.log((132 > 100 && 45 < 12 ) && (132 > 100 || 45 < 12 ) );
// console.log((111 > 11 || 45 < 111) || (99 > 100 && 45 > 12 ));
// console.log(!!'-1');
// console.log(!!-1);
// console.log(!!'0');
// console.log(!!'null');
// console.log(!!'undefined');
// console.log(!!(3/'owu'));
// console.log((111 > 11 || 45 < 111) ||  !!'0');
// console.log((!!111 == !!11 || 45 < 111) && (99 > 100 && 45 > 12 ));