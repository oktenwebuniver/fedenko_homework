// - создать 5 объектов. В каждом объекте не менее 3х полей. Все объекты разные по набору полей. (Т.е поле
//     name  должно присутствовать только 1 раз в одном объекте )
const users1 = {
   name: 'Maksym',
   pass: '12345',
   phone: '0976322866',
   isMerid: true,
   car: false,
};
const users2 = {
   name: 'Alina',
   pass: '12345',
   phone: '0653405776',
   isMerid: true,
   car: true,
};
const users3 = {
   name: 'Tamara',
   pass: '12345',
   phone: '0632114955',
   isMerid: false,
   car: false,
};
const users4 = {
   name: 'Ivan',
   pass: 'qwer1234',
   phone: '0967777777',
   isMerid: false,
   car: true,
};

//    - создать 5 объектов с полностью разным набором полей. В каждом объекте должен присутсвовать массив и
//    внутренний объект. Опишите что угодно, машину, картину, болт... Пример : let man = { name: 'kokos',
//    skills : ['java','js'] , wife: { name: 'cherry' } };
const man = {
   name: 'kokos',
   skills: ['java', 'js'],
   wife: { name: 'cherry' },
};

const student = {
   university: 'OWU',
   city: ['kyiv', 'lviv'],
   perents: { father: 'Vika', mother: 'Vova' },
};
const car = {
   location: 'Kyiv',
   audio: true,
   color: { top: 'black', bottom: 'white', doors: 'pink' },
};
const tShirt = {
   color: 'blue',
   size: { height: 90, whith: 50 },
   print: ['dog', 'cat', 'circle'],
};

//    - При помощи for in вывести все ключи всех объектов из задания 1 и 2
// for (const key in users1) {
//    console.log(key);}
// for (const key in users2) {
//    console.log(key);}
// for (const key in users3) {
//    console.log(key);}
// for (const key in users4) {
//    console.log(key);}
// for (const key in man) {
//    console.log(key);}
// for (const key in student) {
//    console.log(key);}
// for (const key in car) {
//    console.log(key);}
// for (const key in tShirt) {
//    console.log(key);}

//    - При помощи Object.keys вывести все ключи всех объектов из задания 1 и 2
// console.log(keys(users1));
// console.log(users2.keys());
// console.log(users3.keys());
// console.log(users4.keys());
//    - Создать массив из 10 объектов cars и заполнить его машинами с полями модель, год выпуска, мощность,
//    цвет. (Значаения полей могу быть выдуманными)
const arrayCars2 = [
   { model: 'bentli', madeYear: 2015, power: 420 },
   { model: 'tesla:', madeYear: 2020, power: 355 },
   { model: 'vaz', madeYear: 2010, power: 212 },
   { model: 'audi', madeYear: 2012, power: 263 },
   { model: 'niva', madeYear: 2005, power: 200 },
   { model: 'opel', madeYear: 2015, power: 287 },
   { model: 'zaz', madeYear: 2010, power: 154 },
   { model: 'mitsubisi', madeYear: 2015, power: 312 },
   { model: 'alfraomeo', madeYear: 2018, power: 385 },
   { model: 'toyota', madeYear: 2003, power: 208 },
];
//    - Создать массив объектов cities и заполнить его объектами с полями название, популяция, страна, регион.
//     (Значаения полей могу быть выдуманными)
const people = [
   { population: 4000000, country: 'Ukrain', region: 'Kyiv' },
   { population: 22000000, country: 'China', region: 'Pekin' },
   { population: 7000000, country: 'Italia', region: 'Rom' },
   { population: 6000000, country: 'Spain', region: 'Madrid' },
   { population: 15000000, country: 'India', region: 'Tynis' },
];
// - Создать массив объектов cars и заполнить его машинами с полями модель, год выпуска, мощность, цвет,
// водитель. Водитель является отдельным объектом с полями имя, возраст, пол, стаж.
// const arrayCars = [
//    { model: 'bentli', madeYear: 2015, power: 420, color: 'black', driver:
//       {name: 'Max', age: 32, gender: 'man', experience: true} },
//    { model: 'tesla:', madeYear: 2020, power: 355, color: 'pink', driver:
//       {name: 'Eduard', age: 28, gender: 'man', experience: false} },
//    { model: 'vaz', madeYear: 2010, power: 212, color: 'red', driver:
//       {name: 'Alfred', age: 35, gender: 'man', experience: true} },
//    { model: 'audi', madeYear: 2012, power: 263, color: 'white', driver:
//       {name: 'Vika', age: 18, gender: 'woman', experience: false} },
//    { model: 'niva', madeYear: 2005, power: 200, color: 'blue', driver:
//       {name: 'Alina', age: 25, gender: 'woman', experience: true} },
//    { model: 'opel', madeYear: 2015, power: 287, color: 'yellow', driver:
//       {name: 'Anton', age: 27, gender: 'man', experience: true} },
//    { model: 'zaz', madeYear: 2010, power: 154, color: 'black', driver:
//       {name: 'Dima', age: 35, gender: 'man', experience: true} },
//    { model: 'mitsubisi', madeYear: 2015, power: 312, color: 'yellow', driver:
//       {name: 'Ann', age: 18, gender: 'woman', experience: false} },
//    { model: 'alfraomeo', madeYear: 2018, power: 385, color: 'broun', driver:
//       {name: 'Adolf', age: 20, gender: 'man', experience: true} },
//    { model: 'toyota', madeYear: 2003, power: 208, color: 'red', driver:
//       {name: 'Lisa', age: 25, gender: 'woman', experience: false} },
// ];

//

//    - проитерировать каждый массив из задания 5,6,7 при помощи while.
let i = 0;
// while (i < arrayCars2.length) {
//    console.log(arrayCars2[i]);
//    i++;
// }
// i = 0;
// while (i < people.length) {
//    console.log(people[i]);
//    i++;}
// i = 0;
// while (i < arrayCars.length) {
//    console.log(arrayCars[i]);
//    i++;}

//

//    - проитерировать каждый массив из задания 5,6,7 при помощи for.
// for (let i = 0; i < arrayCars2.length; i++) {
//    const element = arrayCars2[i];
//    console.log(element);}
// for (let i = 0; i < people.length; i++) {
//    const element = people[i];
//    console.log(element);}
// for (let i = 0; i < arrayCars.length; i++) {
//    const element = arrayCars[i];
//    console.log(element);}

//

//    - проитерировать каждый массив из задания 5,6,7 при помощи  for of.
// for (const i of arrayCars2) {
//    console.log(i);}
// for (const i of people) {
//    console.log(i);}
// for (const i of arrayCars) {
//    console.log(i);}

//

//    - взять объекты из задания 1 и превратить каждый в json.
// let users1JSON = JSON.stringify(users1);
// let users2JSON = JSON.stringify(users2);
// let users3JSON = JSON.stringify(users3);
// let users4JSON = JSON.stringify(users4);

//

//    - взять json из задания 11 и превратить их обратно в объекты.
// const users1new = JSON.parse(users1JSON);
// const users2new = JSON.parse(users2JSON);
// const users3new = JSON.parse(users3JSON);
// const users4new = JSON.parse(users4JSON);

//

//    - взять массив из задания 5,в цикле перебрать его объекты превратив их в json .
// for (const i of arrayCars2) {
//    JSON.stringify(i);
// }

//

//    - взять массив из задания 6,в цикле перебрать его объекты превратив их в json .
// for (const i of people) {
//    JSON.stringify(i);
// }

//

//    - взять массив из задания 7,в цикле перебрать его объекты превратив их в json и сразу скоприовать в
//    новый массив.
// const arrCarNewJSON = [];
// for (const i of arrayCars) {
//    const itemJSON = JSON.stringify(i);
//    arrCarNewJSON.push(itemJSON);
// }

//

// =============================================================
// =============================================================

//    - Создать массив пользователей. У каждого пользователя обязательно должено быть поле skills которое
//    является массивом. Проитерировать массив пользователей и в каждом пользователе проитерировать
//    его массив skills
let users0 = [
   { name: 'Vasya', age: 31, status: false, skills: ['java', 'js'] },
   { name: 'Petya', age: 30, status: true, skills: ['java', 'js', 'html'] },
   { name: 'Kolya', age: 29, status: true, skills: ['mysql', ',mongo'] },
   { name: 'Olya', age: 28, status: false, skills: ['java', 'js'] },
   { name: 'Maksym', age: 30, status: true, skills: ['mysql', ',mongo'] },
];
//    - Создать массив пользователей. У каждого пользователя обязательно должено быть поле skills которое
//    является массивом. Проитерировать массив пользователей и в каждом пользователе проитерировать его
//    массив skills. Скопировать все skills всех пользователей в отедльный массив
const arrUsersSkills = [];
const arrUsersName = [];
for (const user of users0) {
   arrUsersName.push(user.name);
   arrUsersSkills.push(user.skills);
}
for (let i = 0; i < arrUsersName.length; i++) {
   const person = arrUsersName[i];
   const skills = arrUsersSkills[i];
   //console.log(`Студент ${person} має такі навички: ${skills}.`);
}
//console.log(arrUsersSkills);

//    - За допомогою 2х циклів циклів проітеррувати  даний масив і масив кожного об'єкта.
for (const user of users0) {
   //console.log(user);
   for (const skill of user.skills) {
      //console.log(skill);
   }
}

//    - З масиву users за допомогою циклу витягнути адреси користувачів і записати (скопіювати) їх в
//    інший порожній масив.
let users = [
   {
      name: 'vasya',
      age: 31,
      status: false,
      address: {
         city: 'Lviv',
         country: 'Ukraine',
         street: 'Shevchenko',
         houseNumber: 1,
      },
   },
   {
      name: 'petya',
      age: 30,
      status: true,
      address: {
         city: 'New York',
         country: 'USA',
         street: 'East str',
         houseNumber: 21,
      },
   },
   {
      name: 'kolya',
      age: 29,
      status: true,
      address: {
         city: 'Budapest',
         country: 'Hungary',
         street: 'Kuraku',
         houseNumber: 78,
      },
   },
   {
      name: 'olya',
      age: 28,
      status: false,
      address: {
         city: 'Prague',
         country: 'Czech',
         street: 'Paster',
         houseNumber: 56,
      },
   },
   {
      name: 'max',
      age: 30,
      status: true,
      address: {
         city: 'Istanbul',
         country: 'Turkey',
         street: 'Mikar',
         houseNumber: 39,
      },
   },
   {
      name: 'anya',
      age: 31,
      status: false,
      address: {
         city: 'Rio',
         country: 'Brasil',
         street: 'Ronaldi',
         houseNumber: 5,
      },
   },
   {
      name: 'oleg',
      age: 28,
      status: false,
      address: {
         city: 'Montreal',
         country: 'Canada',
         street: 'Acusto',
         houseNumber: 90,
      },
   },
   {
      name: 'andrey',
      age: 29,
      status: true,
      address: {
         city: 'Quebeck',
         country: 'Canada',
         street: 'Binaro',
         houseNumber: 33,
      },
   },
   {
      name: 'masha',
      age: 30,
      status: true,
      address: {
         city: 'Moscow',
         country: 'Russia',
         street: 'Gogolia',
         houseNumber: 1,
      },
   },
   {
      name: 'olya',
      age: 31,
      status: false,
      address: {
         city: 'Portland',
         country: 'USA',
         street: 'Forest str',
         houseNumber: 4,
      },
   },
   {
      name: 'max',
      age: 31,
      status: true,
      address: {
         city: 'Cairo',
         country: 'Egypt',
         street: 'Seashore',
         houseNumber: 45,
      },
   },
];
// const usersAdresses = [];
// for (const user of users) {
//    for (const address in user.address) {
//       usersAdresses.push(address);
//    }
// }
//console.log(usersAdresses);

//

//    - За допомоги циклу проітерувати  масив users,
// записати кожного юзера в сівй блок за допомоги
// document.createElement. Всі данні в одному блоці.
// for (const user of users) {
//    const block = document.createElement('div');
//    block.innerText = `Користувач ${user.name}. ${user.age} років. Проживає в ${user.address.city}`;
//    document.body.appendChild(block);
//    block.classList.add('block');
// }

//

//    - За допомоги циклу проітерувати  масив users, записати кожного юзера в сівй блок за допомоги
//    document.createElement, розділивши всі властивості по своїм блокам (div>div*4)
// for (let i = 0; i < users.length; i++) {
//    const user = users[i];
//    const div = document.createElement('div');
//    const h3 = document.createElement('h3');
//    const p1 = document.createElement('p');
//    const p2 = document.createElement('p');
//    let address = '';
//    for (const address1 in user.address) {
//       address = address + ' ' + user.address[address1];}
//    h3.innerText = `Користувач - ${user.name}`;
//    p1.innerText = `вік ${user.age}`;
//    p2.innerText = user.address;
//    div.classList.add('block')
//    document.body.appendChild(div);
//    div.appendChild(h3);
//    div.appendChild(p1);
//    div.appendChild(p2);
// }

//

//    - За допомоги циклу проітерувати  масив users, записати кожного юзера в сівй блок за допомоги
//    document.createElement, розділивши всі властивості по своїм блокам , блок з адресою зробити окремим
//     блоком, з блоками для кожної властивості
// for (let i = 0; i < users.length; i++) {
//    const user = users[i];
//    const div = document.createElement('div');
//    const usname = document.createElement('div');
//    const usage = document.createElement('div');
//    const usaddr = document.createElement('div');
//    const addrcity = document.createElement('div');
//    const addrcountry = document.createElement('div');
//    const addrstreet = document.createElement('div');
//    const addrhouseNumbe = document.createElement('div');
//    usname.innerText = `Користувач - ${user.name}`;
//    usage.innerText = `вік ${user.age}`;
//    usaddr.innerText = 'Адреса:';
//    addrcountry.innerText = user.address.country;
//    addrcity.innerText = user.address.city;
//    addrstreet.innerText = user.address.street;
//    addrhouseNumbe.innerText = user.address.houseNumber;
//    addrcountry.classList.add('item');
//    addrcity.classList.add('item2');
//    addrstreet.classList.add('item');
//    addrhouseNumbe.classList.add('item2');
//    div.classList.add('block');
//    document.body.appendChild(div);
//    div.appendChild(usname);
//    div.appendChild(usage);
//    div.appendChild(usaddr);
//    usaddr.appendChild(addrcountry);
//    usaddr.appendChild(addrcity);
//    usaddr.appendChild(addrstreet);
//    usaddr.appendChild(addrhouseNumbe);
// }

//

//    - Дано 2 масиви з рівною кількістю об'єктів.
//    Масиви:
let usersWithId = [
   { id: 1, name: 'vasya', age: 31, status: false },
   { id: 2, name: 'petya', age: 30, status: true },
   { id: 3, name: 'kolya', age: 29, status: true },
   { id: 4, name: 'olya', age: 28, status: false },
];
let citiesWithId = [
   { userId: 3, country: 'USA', city: 'Portland' },
   { userId: 1, country: 'Ukraine', city: 'Ternopil' },
   { userId: 2, country: 'Poland', city: 'Krakow' },
   { userId: 4, country: 'USA', city: 'Miami' },
];
//    З'єднати в один об'єкт користувача та місто з відповідними "id" та "user_id" .
//    Записати цей об'єкт в новий масив
//    Частковий приклад реультату:
//    let usersWithCities = [{id: 1, name: 'vasya', age: 31, status: false, address: {user_id: 1,
//    country: 'Ukraine', city: 'Ternopil'}}....]
for (const user of usersWithId) {
   for (const city of citiesWithId) {
      if (user.id === city.userId) {
         user.address = city;
      }
   }
}
console.log(usersWithId);

//

//    - створити розмітці блок з id, class та текстом в середені. Зчитати окремо цей текст з
//    селекторів по id , class та тегу
// const withId = document.getElementById('content');
// console.log(withId.innerText);
//
// const withClass = document.getElementsByClassName('rules');
// for (const item of withClass) {
//    console.log(item.innerText);
// }
// const withTag = document.getElementsByTagName('h2');
//    for (const item of withTag) {
//       console.log(item.innerText);
//    }

//

//    - змінити цей текст використовуючи селектори id, class,  tag
// const withId = document.getElementById('content');
// console.log(withId.innerText = 'змінити цей текст використовуючи селектори id');

// const withTag = document.getElementsByTagName('h2');
//    for (const it of withTag) {
//       console.log(it.innerText = 'змінити цей текст використовуючи селектори tag');
//    }

// const withClass = document.getElementsByClassName('rules');
// for (const item of withClass) {
//    console.log(item.innerText = 'змінити цей текст використовуючи селектори class');
// }

//

//    - змінити висоту та ширину блоку використовуючи селектори id, class,  tag
// const withId = document.getElementById('content');
// withId.style.width = '500px';
// withId.style.height = '20px';
// withId.style.backgroundColor = 'red';

// const withTag = document.getElementsByTagName('h2');
// for (const it of withTag) {
//    it.style.height = '50px';
//    it.style.backgroundColor = 'yellow';
// }

// const withClass = document.getElementsByClassName('rules');
// for (const item of withClass) {
//    item.style.height = '100px';
//    item.style.color = 'blue';
// }

//

//    - за допомоги document.createElement та appendChild створити таблицю на 1 рядок з трьома
//     ячейками всередені
// const perentDiv = document.createElement('div');
// perentDiv.classList.add('wraper');
// document.body.appendChild(perentDiv);
// for (let i = 0; i < 3; i++) {
//    const div = document.createElement('div');
//    div.classList.add('tabItem');
//    perentDiv.appendChild(div);
// }

//

//    - за допомоги document.createElement, appendChild та циклу створити таблицю на 10
//    рядків з трьома ячейками всередені
// const perentDiv = document.createElement('div');
// perentDiv.classList.add('wraper');
// document.body.appendChild(perentDiv);
// for (let i = 0; i < 20; i++) {
//    const div = document.createElement('div');
//    div.classList.add('tabItem');
//    perentDiv.appendChild(div);
// }

//

//    - за допомоги document.createElement, appendChild та 2х циклів створити таблицю на
//    10 рядків з 5 ячейками всередені
// const table = document.createElement('table');
// for (let i = 0; i < 10; i++) {
//    const tr = document.createElement('tr');
//    for (let i = 0; i < 3; i++) {
//       const td = document.createElement('td');
//       tr.appendChild(td);
//    }
//    table.appendChild(tr);
// }
// document.body.appendChild(table);

//

//    - за допомоги document.createElement, appendChild та 2х циклів створити таблицю на n рядків
//     з m ячейками всередені. n та m отримати з prompt
// const n = +prompt('Ведыть кількість рядків');
// const m = +prompt('Ведыть кількість стовпців');
// const table = document.createElement('table');
// for (let j = 1; j <= n; j++) {
//    const tr = document.createElement('tr');
//    tr.innerText = `столбец ${j}`;
//    table.appendChild(tr);
//    for (let i = 1; i <= m; i++) {
//       const td = document.createElement('td');
//       td.innerText = `строка ${i}`;
//       tr.appendChild(td);
//    }
// }
// document.body.appendChild(table);







//    --Завантажити з мережі будь-який шаблон сайту. Підключити до нього свій скріпт-файл.
//     У файлі прописати наступні доступи та дії
//     - знайти всі елементі, які мають class
// const allElements = document.getElementsByTagName('*');
// for (const item of allElements) {
//    if (item.getAttribute('class')) {
//       console.log(item);
//    }
// }

//     - знайти всі параграфи ,та змінити текст на hello oktenweb!
// const allElements = document.getElementsByTagName('p');
// for (const item of allElements) {
//    item.innerText = 'hello oktenweb!';
// }

//     - знайти всі div та змінити ім колір на червоний
// const allElements = document.getElementsByTagName('div');
// for (const item of allElements) {
//    item.style.backgroundColor = 'red';
//    item.style.margin = '10px';
// }
