// -- створити об'єкт (не меньше 5ти властивостей) який описує
// - собаку
// - людину
// - автомобіль
// - квартиру
// - книгу
const object = {
   dog: true,
   person: 'student',
   car: 'lada',
   apartment: '70m',
   book: 200,
};

//

//

// -- Створити масив та вивести його в консоль:
// - з 5 собак
// - 3 5 людей
// - з 5 автомобілів
const arrDogs = [
   'Golden Retriever',
   'Border Collie',
   'Finnish Spitz',
   'Afghan Hound',
   'Affenpinscher',
];
const arrUsers = [
   'Админ',
   'Феденко Максим',
   'Блажкыв Тарас',
   'Берченко Дима',
   'Гость',
];
const arrCars = [
   'Maserati', 
   'Mercedes', 
   'Ford', 
   'Porsche', 
   'Dodge'
];
//  console.log(arrDogs);
//  console.log(arrUsers);
//  console.log(arrCars);

//

//

// -- створити об'єкт (не меньше 5ти властивостей) який описує, одна з властивостей обов'язково повинна бути об'єктом,
// ще одна - масивом
// - будинок
// - водій
// - іграшку
// - стіл
// - сумка
const object1 = {
   house: {
      window: true,
      garage: false,
      prise: 50000,
      floors: 2,
   },
   driver: [
      { name: 'maksym', car: true, license: false },
      { name: 'ali', car: true, license: true },
   ],
   toy: 'small',
   table: true,
   bag: 'big',
};

//

//

// Дан массив:
let users = [
   { name: 'vasya', age: 31, status: false },
   { name: 'petya', age: 30, status: true },
   { name: 'kolya', age: 29, status: true },
   { name: 'olya', age: 28, status: false },
   { name: 'max', age: 30, status: true },
   { name: 'anya', age: 31, status: false },
   { name: 'oleg', age: 28, status: false },
   { name: 'andrey', age: 29, status: true },
   { name: 'masha', age: 30, status: true },
   { name: 'olya', age: 31, status: false },
   { name: 'max', age: 31, status: true },
];
// Приклад: вивести ім'я 1го об'єкта. Відповідь: console.log (users [0] .name). Будьте уважні! 4й об'єкт має індес 3!
// - звернутися в відповідну ячейку масиву і відповідний параметр об'єкта і вивести в консольх наступне
// - статус Андрія
// console.log(users[0].status);
// // - статус Максима
// console.log(users[4].status);
// console.log(users[10].status);
// // - ім'я передостаннього об'єкту
// console.log(users[9].name);
// // - ім'я третього об'єкта
// console.log(users[2].name);
// // - вік Олега
// console.log(users[6].age);
// // - вік Олі
// console.log(users[3].age);
// console.log(users[9].age);
// console.log(users[3].age);
// // - вік + ім'я 5го об'єкта
// console.log(users[4].age);
// console.log(users[4].name);
// // - вік + сатус Анни
// console.log(users[5].age);
// console.log(users[5].status);

//

//

// -Візьміть файл template1.html, підключіть до нього скрипт, і працюйте в ньому.

// -- Напишіть код,  який за допомоги document.getElementById або
//document.getElementsByClassName або document.getElementsByTagName :
// - отримує текст з параграфа з id "content"
const content = document.getElementById('content');

// - отримує текст з блоку з id "rules"
const rules = document.getElementById('rules');

// - замініть текст параграфа з id 'content' на будь-який інший
content.innerText = "будь-який інший для параграфа з id 'content'";

// - замініть текст параграфа з id 'rules' на будь-який інший
rules.innerText = "будь-який інший для параграфа з id 'rules'";

// - змініть кожному елементу колір фону на червоний
content.classList.add('red');
rules.classList.add('red');

//змініть кожному елементу колір тексту на синій
content.classList.add('blue');
rules.classList.add('blue');

// - отримати весь список класів елемента з id=rules і вивести їх в console.log
//console.log(rules.classList);

// - отримати всі елементи з класом fc_rules
const fcRules = document.getElementsByClassName('fc_rules');

// - поміняти колір тексту у всіх елементів fc_rules на червоний
for (let i of fcRules) {
   i.classList.add('textRed');
}
//console.log(fcRules);
//                                       ___________IT DOES NOT WORK___________
// for (const key in fcRules) {
//    const element = fcRules[key];
//    console.log(element);
//    element.classList.add('textRed');
// }
