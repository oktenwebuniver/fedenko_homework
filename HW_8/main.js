// - Дана textarea.
// В неё вводится текст.
// Сделайте так, чтобы после захода на эту страницу через некоторое время, введенный
// текст остался в textarea.

// const area1 = document.getElementById('task1');
// const btn1 = document.getElementById('btnTask1_1');
// const btn2 = document.getElementById('btnTask1_2');

// area1.value = localStorage.getItem('myText');
// btn1.onclick = () => localStorage.setItem('myText', area1.value);

// btn2.onclick = () => {
//   localStorage.removeItem('myText');
//   area1.value = '';
// };

//

//

// - Дана форма с инпутами, текстареа, чекбоксами, радио кнопочками, селектами и тп.
// Пользователь вводит какие-то данные и закрывает страницу (не факт, что он заполнил всю форму).
// Сделайте так, чтобы при следующем заходе на страницу введенные им ранее данные
// стояли на своих местах.
// Сделайте ваш скрипт как можно более универсальным.

// const allsTag = document.querySelectorAll('*');
// const form1 = document.getElementById('form-task2');
// getDataForm(allsTag);
// function saveForm(t) {
//   setDataForm(t);
// }

// function setDataForm(tag) {
//   for (let i = 0; i < tag.length; i++) {
//     const tagElement = tag[i];
//     if (tagElement.type === 'button') {
//       continue;
//     }
//     if (tagElement.type === 'checkbox' || tagElement.type === 'radio') {
//       if (tagElement.checked) {
//         tagElement.value = true;
//       } else {
//         tagElement.value = false;
//       }
//     }
//     localStorage.setItem(tagElement.id, tagElement.value);
//   }
// }

// function getDataForm(allTag) {
//   for (let i = 0; i < allTag.length; i++) {
//   // console.log(allTag[i]);

//     if (allTag[i].id && localStorage.hasOwnProperty(allTag[i].id)) {
//       console.log(allTag[i]);
//       if (allTag[i].value === 'true') {
//         allTag[i].setAttribute('checked', 'checked');
//       }
//     }
//   }
// }

//

//

// -Дан текстареа. В него можно ввести данные, нажать кнопку "сохранить" и они
// "фикисруются" (в хранилище), затем поредактировать их, затем еще поредактировать и возможно еще.....
// Требование : хранить историю своих изменений (даже после перезагрузки страницы).
// Сверху над текстареа должны появится стрелочки, с помощью которых можно
// перемещаться по истории (не забудьте!чекпоинт истории - нажатеи кнопки сохранить).

// const backBtn = document.querySelector('.back');
// const nextBtn = document.querySelector('.next');
// const textArea3 = document.querySelector('#textArea3');
// const saveBtn = document.querySelector('.saveBtn');

// saveBtn.onclick = () => {
//   localStorage.setItem(localStorage.length + 1, textArea3.value);
// };

// backBtn.onclick = () => {
//   nextBtn.style.visibility = 'visible';
//   let index;
//   for (const key in localStorage) {
//     if (localStorage.hasOwnProperty(key)) {
//       const element = localStorage.getItem(key);
//       if (element === textArea3.value) {
//         index = key;
//       }
//     }
//   }
//   if (index === '1') {
//     backBtn.style.visibility = 'hidden';
//     return;
//   }
//   textArea3.value = localStorage.getItem(index - 1);
// };

// nextBtn.onclick = () => {
//   backBtn.style.visibility = 'visible';
//   let index;
//   for (const key in localStorage) {
//     if (localStorage.hasOwnProperty(key)) {
//       const element = localStorage.getItem(key);
//       if (element === textArea3.value) {
//         index = key;
//       }
//     }
//   }
//   if (index === localStorage.length.toString()) {
//     nextBtn.style.visibility = 'hidden';
//     return;
//   }
//   textArea3.value = localStorage.getItem(+index + 1);
// };

//

//

// - Реализуйте записную книгу, хранящую данные в локальном хранилище.
// Данные которые надо сохранять : ФИО, номер, почта, фирма, отдел, день рождения
// Данные вводить через соответсвующую форму.
// --Каждому контакту добавить кнопку для удаления контакта.
// --Каждому контакту добавить кнопку редактироваиня. При нажати на нее появляется
// форма, в которой есть все необходимые инпуты для редактирования, которые уже
// заполнены данными объекта

let tempUser = {};
const form = document.forms.form;
const submit1 = document.getElementById('createBtn');
const wraperUsers = document.getElementsByClassName('wraperUsers')[0];
const ARR_USERS = 'ARR_USERS';

getUsersFromLS();

submit1.onclick = (e) => {
  // e.preventDefault();
  let person = { ...tempUser };
  for (let i = 0; i < form.children.length; i++) {
    const element = form.children[i];
    if (element.name) {
      person[element.name] = element.value;
    }
  }
  if (!person.id) {
    person.id = new Date().getTime();
  }
  saveUser(person);
  // location.reload();
};

function saveUser(user) {
  if (localStorage.hasOwnProperty(ARR_USERS)) {
    const arrayUsers = JSON.parse(localStorage.getItem(ARR_USERS));
    const find = arrayUsers.find((value) => value.id === user.id);

    if (find) {
      const filter = arrayUsers.filter((value) => value.id !== user.id);
      filter.push(user);
      localStorage.setItem(ARR_USERS, JSON.stringify(filter));
    } else {
      arrayUsers.push(user);
      localStorage.setItem(ARR_USERS, JSON.stringify(arrayUsers));
    }
  } else {
    localStorage.setItem(ARR_USERS, JSON.stringify([user]));
  }
}

function getUsersFromLS() {
  if (localStorage.hasOwnProperty(ARR_USERS)) {
    const arrUser = JSON.parse(localStorage.getItem(ARR_USERS));
    for (const user of arrUser) {
      createUserDiv(user);
    }
  }
}

function createUserDiv(user) {
  const userDiv = document.createElement('div');
  userDiv.classList.add('userDiv');
  let flag = true;
  for (const key in user) {
    if (flag) {
      const h3 = document.createElement('h3');
      h3.innerText = `${key} : ${user[key]}`;
      userDiv.appendChild(h3);
      flag = false;
      continue;
    }
    if (key === 'id') {
      continue;
    }
    const p = document.createElement('p');
    p.innerText = `${key} : ${user[key]}`;
    userDiv.appendChild(p);
  }
  const edit = document.createElement('button');
  const delite = document.createElement('button');
  edit.innerText = 'Edit';
  delite.innerText = 'Delete';

  edit.onclick = () => {
    editUser(user.id);
  };

  delite.onclick = () => {
    deleteUser(user.id);
  };

  userDiv.appendChild(edit);
  userDiv.appendChild(delite);
  wraperUsers.appendChild(userDiv);
}

function deleteUser(id) {
  const parse = JSON.parse(localStorage.getItem(ARR_USERS));
  const filter = parse.filter((user) => user.id !== id);
  localStorage.setItem(ARR_USERS, JSON.stringify(filter));
  location.reload();
}

function editUser(id) {
  const parse = JSON.parse(localStorage.getItem(ARR_USERS));
  const user = parse.find((user) => user.id === id);

  for (let i = 0; i < form.children.length; i++) {
    const element = form.children[i];
    if (element.name) {
      for (const key in user) {
        if (element.name === key) {
          element.value = user[key];
        }
      }
    }
  }
  tempUser = user;
}
