// - создать массив с 20 числами.
const numbersArray = [23, 45, 34, 24, 56, 77, 44, 100, 0, 33, 10, 52, 28, 36];
// console.log(numbersArray);

//

// -- при помощи метода sort и колбека  отсортировать массив.
const sortArr1 = JSON.parse(JSON.stringify(numbersArray));
sortArr1.sort((a, b) => a - b);
// console.log(sortArr1);

//

// -- при помощи метода sort и колбека отсортировать массив в ниспадающем напралении.
const sortArr2 = JSON.parse(JSON.stringify(numbersArray));
sortArr2.sort((a, b) => b - a);
// console.log(sortArr2);

//

// -- при помощи filter получить числа кратные 3
const filterArr3 = numbersArray.filter((value) => value % 3 === 0);
// console.log(filterArr3);

//

// -- при помощи filter получить числа кратные 10
const filterArr10 = numbersArray.filter((value) => value % 10 === 0);
// console.log(filterArr10);

//

// -- перебрать (проитерировать) массив при помощи foreach()
// numbersArray.forEach((value, index, array) => console.log(`значення ${value} та індекс ${index} з масиву: ${array}`));

//

// -- перебрать массив при помощи map() и получить новый массив в котором все значения будут в 3 раза больше
const mapArr = numbersArray.map((value) => value * 3);
// console.log(mapArr);

//

// - создать массив со словами на 15-20 элементов.
const stringsArray = [
   'feden',
   'maks',
   'owu',
   'git',
   'kyiv',
   'romny',
   'university',
   'alina',
   'item',
   'lviv',
   'ukrain',
   'homster',
   'blood',
   'sven',
   'top',
   'bot',
   'tamara',
   'body',
   'arsenal',
];

//

// -- отсортировать его по алфавиту в восходящем порядке.
const sortString1 = JSON.parse(JSON.stringify(stringsArray));
sortString1.sort((a, b) => {
   if (a < b) {
      return -1;
   }
   return 1;
});
// console.log(sortString1);

//

// -- отсортировать его по алфавиту  в нисходящем порядке.
const sortString2 = JSON.parse(JSON.stringify(stringsArray));
sortString2.sort((a, b) => {
   if (a > b) {
      return -1;
   }
   return 1;
});
// console.log(sortString2);

//

// -- отфильтровать слова длиной менее 4х символов
const smallerWords = stringsArray.filter((value) => value.length < 4);
// console.log(smallerWords);

//

// -- перебрать массив при помощи map() и получить новый массив в котором все значения будут со знаком "!" в конце
const mapArray = stringsArray.map((value) => `${value}!`);
// console.log(mapArray);

//

// Все робити через функції масивів (foreach, map ...тд)
// Дан масив :
let users = [
   { name: 'vasya', age: 31, status: false },
   { name: 'petya', age: 30, status: true },
   { name: 'kolya', age: 29, status: true },
   { name: 'olya', age: 28, status: false },
   { name: 'max', age: 30, status: true },
   { name: 'anya', age: 31, status: false },
   { name: 'oleg', age: 28, status: false },
   { name: 'andrey', age: 29, status: true },
   { name: 'masha', age: 30, status: true },
   { name: 'olya', age: 31, status: false },
   { name: 'max', age: 31, status: true },
];
// - відсортувати його за  віком (зростання , а потім окремо спадання)
const sortUsersAgeUp = JSON.parse(JSON.stringify(users));
sortUsersAgeUp.sort((a, b) => a.age > b.age);
// console.log(sortUsersAgeUp);

const sortUsersAgeDown = JSON.parse(JSON.stringify(users));
sortUsersAgeDown.sort((a, b) => a.age < b.age);
// console.log(sortUsersAgeDown);

//

// - відсортувати його за кількістю знаків в імені  (зростання , а потім окремо спадання)
const sortUsersNameUp = JSON.parse(JSON.stringify(users));
sortUsersNameUp.sort((a, b) => {
   if (a.name.length < b.name.length) {
      return -1;
   }
   return 1;
});
// console.log(sortUsersNameUp);

const sortUsersNameDown = JSON.parse(JSON.stringify(users));
sortUsersNameDown.sort((a, b) => {
   if (a.name.length > b.name.length) {
      return -1;
   }
   return 1;
});
// console.log(sortUsersNameDown);

//

// - пройтись по ньому та додати кожному юзеру поле id - яке характеризує унікальний індентифікатор (По якому принципу
//   його створювати - ваше рішення), та зберегти це в новий масив (первинний масив залишиться без змін)
const usersWithId = JSON.parse(JSON.stringify(users));
usersWithId.map((user, index) => (user.id = index + 1));
// console.log(usersWithId);

//

// - відсортувати його за індентифікатором
const usersWithId1 = JSON.parse(JSON.stringify(usersWithId));
usersWithId1.sort((a, b) => a.id < b.id);
// console.log(usersWithId1);

//

// -- наисать функцию калькулятора с 2мя числами и колбеком
const calc2nums = (a, b, callBack) => {
   return callBack(a, b);
};

let plus = calc2nums(2, 2, (a, b) => {
   return a + b;
});
console.log(plus);

//

// -- наисать функцию калькулятора с 3мя числами и колбеком
const calc3nums = (a, b, c, callBack) => {
   return callBack(a, b, c);
};

let multip = calc3nums(2, 2, 2, (a, b, c) => {
   let res = a * b * c;
   return res;
});
console.log(multip);
