'use strict';
// --створити масив та вивести його в консоль:
// - з 5 числових значень
// - з 5 стічкових значень
// - з 5 значень стрічкового, числового та булевого типу
const array1 = [1, 2, 3, 4, 5];
const array2 = ['1', '2', '3', '4', '5'];
const array3 = ['1', '2', '3', '4', '5', 125, true];

// console.log(array1);
// console.log(array2);
// console.log(array3);

//

// -- Створити пустий масив. Наповнити його будь-якими значеннями звертаючись до конкретного індексу. Вивести в консоль
const array4 = [];
array4[0] = 'Fedenko';
array4[1] = 'Maksym';
array4[2] = null;
array4[3] = 'isMarried';

//console.log(array4);

//

// - За допомогою циклу for і document.write() вивести 10 блоків div c довільним текстом всередині
for (let i = 0; i < 10; i++) {
   //document.write(`<div>div з довільним текстом всередині</div>`);
}

//

// - За допомогою циклу for і document.write() вивести 10 блоків div c довільним текстом і індексом всередині
for (let i = 0; i < 10; i++) {
   //  document.write(`<div>${i+1}-й div з довільним текстом всередині</div>`);
}

//

// - За допомогою циклу while вивести в документ 20 блоків h1 c довільним текстом всередині.
let i = 0;
while (i < 20) {
   //document.write(`<h1>h1 з довільним текстом всередині</h1>`);
   i++;
}

//

// - За допомогою циклу while вивести в документ 20 блоків h1 c довільним текстом і індексом всередині.
i = 0;
while (i < 20) {
   //document.write(`<h1>${i + 1}-й h1 з довільним текстом всередині</h1>`);
   i++;
}

//

// - Створити масив з 10 числових елементів. Вивести в консоль всі його елементи в циклі.
const array5 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
for (let i = 0; i < array5.length; i++) {
   //console.log(array5[i]);
}
//

// - Створити масив з 10 строкрових елементів. Вивести в консоль всі його елементи в циклі.
const array6 = [
   'Fedenko',
   'Maksym',
   'true',
   '234',
   'false',
   '123456',
   '{ age: 24 }',
   'owu',
   'school',
];
for (let i = 0; i < array6.length; i++) {
   //console.log(array6[i]);
}
//

// - Створити масив з 10 елементів будь-якого типу. Вивести в консоль всі його елементи в циклі.
const array7 = [
   'Fedenko',
   'Maksym',
   true,
   234,
   false,
   123456,
   { age: 24 },
   'owu',
   'school',
];
for (let i = 0; i < array7.length; i++) {
   //console.log(array7[i]);
}
//

// - Створити масив з 10 елементів числового, стірчкового і булевого типу. За допомогою if та typeof вивести тільки
//булеві елементи
const array8 = [
   'Fedenko',
   false,
   'Maksym',
   true,
   234,
   true,
   'hello',
   'world',
   false,
   123456,
   { age: 24 },
   'owu',
   'school',
];
// for (let i = 0; i < array8.length; i++) {
//     if (array8[i] === true || array8[i] === false) {
//         console.log(`${i + 1}-й ${array8[i]}`);
//     }
// }

// for (let i = 0; i < array8.length; i++) {
//     if (typeof(array8[i]) === Boolean) {
//         console.log(`${i + 1}-й ${array8[i]}`);
//     }
// }

//

// - Створити масив з 10 елементів числового, стірчкового і булевого типу. За допомогою if та typeof вивести тільки
//числові елементи
// - Створити масив з 10 елементів числового, стрічкового і булевого типу. За допомогою if та typeof вивести тільки
//рядкові елементи
// - Створити порожній масив. Наповнити його 10 елементами (різними за типами) через звернення до конкретних індексів.
//Вивести в консоль всі його елементи в циклі.
const arr = [];
arr[0] = 'fedenko';
arr[3] = 'maksym';
arr[4] = true;
arr[5] = 12345;
arr[8] = false;
arr[9] = 'OWU';
arr[10] = 8;

for (let i = 0; i < arr.length; i++) {
   // console.log(arr[i]);
}

//

// - Створити цикл for на 10  ітерацій з кроком 1. Вивести поточний номер кроку через console.log та document.write
const array = [];
for (let i = 1; i <= 10; i++) {
   array.push(i);
}
// console.log(`Крок ітерації - ${array[4] - array[3]}`);
// document.write(`Крок ітерації - ${array[4] - array[3]}`);

//

// - Створити цикл for на 100 ітерацій з кроком 1. Вивести поточний номер кроку через console.log та document.write
const array9 = [];
for (let i = 1; i <= 100; i++) {
   array9.push(i);
}
// console.log(`Крок ітерації - ${array9[4] - array9[3]}`);
// document.write(`Крок ітерації - ${array9[4] - array9[3]}`);

//

// - Створити цикл for на 100 ітерацій з кроком 2. Вивести поточний номер кроку через console.log та document.write
const array10 = [];
for (let i = 1; i <= 200; i = i + 2) {
   array10.push(i);
}
// console.log(`Крок ітерації - ${array10[80] - array10[79]}`);
// document.write(`Крок ітерації - ${array10[80] - array10[79]}`);

//

// - Створити цикл for на 100 ітерацій. Вивести тільки парні кроки. через console.log + document.write
for (let i = 0; i < 100; i = i + 2) {
   // console.log(i);
}

//

// - Створити цикл for на 100 ітерацій. Вивести тільки непарні кроки. через console.log + document.write
for (let i = 0; i < 100; i = i + 2) {
   i++;
   // console.log(i);
}

//////////////////////////       ____  NEW  ____               /////////////////////////////////////////////

// - Відтворити роботу годинника, відрахувавши 2 хвилини (2 цикли! 1й - хвилини, 2й - секунди)
// let maxMin = 2;
// for (let min = 0; min <= maxMin; min++) {
//     let sec = 0;
//     if (min < maxMin) {
//         for (; sec < 60; sec++) {
//             console.log(`${min} мин  ${sec} сек`);
//         }
//     } else if (min == maxMin) {
//         console.log(`${min} мин  ${sec} сек`);
//     } else {
//         min++;
//     }
// }

//

// - Відтворити роботу годинника, відрахувавши  2 години 20 хвилини
//(3 цикли! 1й - години, 2й - хвилини, 3й - секунди)

// let hourStop = 2;
// let minStop = 20;

// let hour = 0;
// let min = 0;
// let sec = 0;

// for (; hour < 24; hour++) {
//    if (hour <= hourStop && min <= minStop) {
//       for (; min <= 60; min++) {
//          if (hour === hourStop && min === minStop) {
//             console.log(`${hour} год  ${min} мин  ${sec} сек`);
//             break;
//          } else {
//             if (min === 60) {
//                min = 0;
//                break;
//             }
//             for (; sec <= 60; sec++) {
//                if (sec === 60) {
//                   sec = 0;
//                   break;
//                } else {
//                   console.log(`${hour} год  ${min} мин  ${sec} сек`);
//                }
//             }
//          }
//       }
//    }
// }

//

//

// const maxH = 2;
// for (let h = 0; h <= maxH; h++) {
//   let min = 0;
//   let sec = 0;
//   if (h < maxH) {
//     for (; min <= 60; min++) {
//       sec = 0;
//       if (min < 60) {
//         for (; sec < 60; sec++) {
//           console.log(`${h} год  ${min} мин  ${sec} сек`);
//         }
//       } else if (min === 60) {
//       }
//     }
//   } else if (h === maxH) {
//     for (; min <= 20; min++) {
//       sec = 0;
//       if (min < 20) {
//         for (; sec < 20; sec++) {
//           console.log(`${h} год  ${min} мин  ${sec} сек`);
//         }
//       } else if (min === 20) {
//         console.log(`${h} год  ${min} мин  ${sec} сек`);
//       }
//     }
//   }
// }

//

// Додатково
//=============================
// - Дано масив: [ 'a', 'b', 'c'] . За допомогою циклу for зібрати всі букви в слово.
let word = '';
const arr2 = ['a', 'b', 'c'];

for (i = 0; i < arr2.length; i++) {
   word = word + arr2[i];
}
//console.log(word);
//console.log(typeof(word));

//

// - Дано масив: [ 'a', 'b', 'c'] . За допомогою циклу while зібрати всі букви в слово.
word = '';
const arr3 = ['a', 'b', 'c'];
i = 0;
while (i < arr3.length) {
   word = word + arr3[i];
   i++;
}
// console.log(word);
// console.log(typeof word);

//

// - Дано масив: [ 'a', 'b', 'c'] . За допомогою циклу for of зібрати всі букви в слово.
const ar1 = ['a', 'b', 'c', 'd', 'e'];
let str1 = '';
for (const i of ar1) {
   str1 = `${str1}${i}`;
}
//console.log(str1);

// - Дано масив: [ 'a', 'b', 'c'] . За допомогою циклу for of зібрати всі букви в слово.

// =================
// =================
// =================
// =================

// - Дан масив ['a', 'b', 'c']. Додайте йому в кінець елементи 1, 2, 3 за допомогою циклу.

let arr4 = ['a', 'b', 'c'];

for (let i = 1; i < 4; i++) {
   arr4.push(i);
}
//console.log(arr4);

// - Дан масив [1, 2, 3]. Зробіть з нього новий масив [3, 2, 1].

let arr5 = [1, 2, 3, 4, 5];
let arr6 = [];

let arr5Length = arr5.length;
for (let i = 0; i < arr5Length; i++) {
   arr6[i] = arr5.pop();
}
// console.log(arr6);

//

// - Дан масив [1, 2, 3]. Додайте йому в кінець елементи 4, 5, 6.
let arr7 = [1, 2, 3];

for (let i = 4; i <= 6; i++) {
   arr7.push(i);
}
//console.log(arr7);

//

// - Дан масив [1, 2, 3]. Додайте йому в початок елементи 4, 5, 6.

let arr8 = [1, 2, 3];
for (let i = 6; i >= 4; i--) {
   arr8.unshift(i);
}
//console.log(arr8);

//

// - Дан масив ['js', 'css', 'jq']. Виведіть на екран перший елемент за допомогою shift()
let arr9 = ['js', 'css', 'jq'];
//console.log(arr9.shift());

//

// - Дан масив ['js', 'css', 'jq']. Виведіть на екран останній елемент за допомогою pop()
let arr10 = ['js', 'css', 'jq'];
//console.log(arr9.pop());

//

// - Дан масив [1, 2, 3, 4, 5]. За допомогою методу/функції slice перетворіть масив в [4, 5].
let arr11 = [1, 2, 3, 4, 5];
//console.log(arr11.slice(3));

//

// - Дан масив [1, 2, 3, 4, 5]. За допомогою методу/функції slice перетворіть масив в [1,2].
let arr12 = [1, 2, 3, 4, 5];
//console.log(arr12.slice(0, 2));

//

// - Дан масив [1, 2, 3, 4, 5]. За допомогою методу/функції splice перетворіть масив в [1, 4, 5].
let arr13 = [1, 2, 3, 4, 5];
arr13.splice(1, 2);
//console.log(arr13);

//

// - Дан масив [1, 2, 3, 4, 5]. За допомогою методу/функції splice зробіть з нього масив [1, 2, 3, 'a', 'b', 'c', 4, 5].
let arr14 = [1, 2, 3, 4, 5];
arr14.splice(3, 0, 'a', 'b', 'c');
//console.log(arr14);

//

// - Дан масив [1, 2, 3, 4, 5]. За допомогою методу/функції splice зробіть з нього масив
//[1, 'a', 'b', 2, 3, 4, 'c', 5, 'e'].
let arr15 = [1, 2, 3, 4, 5];
arr15.splice(1, 0, 'a', 'b');
arr15.splice(6, 0, 'c');
arr15.splice(8, 0, 'e');
//console.log(arr15);

//

// - Взяти масив з 10 чисел або створити його. Вивести в консоль тільки ті елементи, значення яких є парними.
const arr16 = [10, 23, 2, 52, 3, 4, 5, 6, 7, 8, 9, 10];
for (let i = 0; i < arr16.length; i++) {
   const item = arr16[i];
   if (item % 2 === 0) {
      //console.log(item);
   }
}

//

// - Взяти масив з 10 чисел або створити його. Створити 2й порожній масив. За допомогою будь-якого циклу та push ()
//скопіювати значення одного масиву в інший
const arr17 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let arrNew17 = [];
for (let i = 0; i < arr17.length; i++) {
   arrNew17.push(arr17[i]);
}
//console.log(arrNew17);

//

// - Взяти масив з 10 чисел або створити його. Створити 2й порожній масив. За допомогою будь-якого циклу скопіювати
//значення одного масиву в інший.
const arr18 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let arrNew18 = [];
for (const i of arr18) {
   arrNew18.push(i);
}
// console.log(arrNew18);